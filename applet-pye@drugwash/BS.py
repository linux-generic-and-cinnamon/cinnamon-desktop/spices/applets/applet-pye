#!/usr/bin/python3 -B
# A-test

import gi
gi.require_version('Gtk', '3.0')
import os
# this is useless, just a reminder of the shit thrown at us
os.environ["PYTHONDONTWRITEBYTECODE"] = "1"
from JsonSettingsWidgets import *
from gi.repository import Gdk, Gio, GLib, Gtk

ipath = os.path.abspath(os.path.dirname(__file__)) + "/icons"
Gtk.IconTheme.get_default().append_search_path(ipath)
itheme = Gtk.IconTheme.get_for_screen(Gdk.Screen.get_default())
globset = None
# translatable strings; those marked 'button' or 'combo' should be kept short
LApy = _("Apply") + ":"
LAps = _("at start")		# button
LApa = _("async")		# button
LApe = _("every")		# button
LSec = _("sec.")
LIsc = _("Icon scroll")	# button
LScr = _("Scroll step")
LBri = _("Brightness")
LClr = _("Color temperature")
LGam = _("Gamma")
LOpt = "<b>" + _("Option") + "</b>"
LMin = "<b>" + _("Minimum") + "</b>"
LMax = "<b>" + _("Maximum") + "</b>"
LSnr = "<b>" + _("Sunrise") + "</b>"
LSns = "<b>" + _("Sunset") + "</b>"
LIcs = "<b>" + _("Icon set") + "</b>"
LDay = "<b>" + _("Day") + "</b>"
LNgt = "<b>" + _("Night") + "</b>"
LCst = "<b>" + _("Custom") + "</b>"
LAut = "<b>" + _("Auto") + "</b>"
LAdj = _("Adjustments")
LDis = _("Disabled")		# combo
LOnl = _("Online")		# combo
LOff = _("Offline")		# combo
LAlw = _("Always start in auto mode")	# button
LDly = _("Delay")
LTim = _("Time")
LEna = _("Enabled")		# button
LNon = _("None")		# combo
LSel = _("Selected")		# combo
LAll = _("All")				# combo
LMds = _("Modes")
LMdc = _("Modes (cloudy on)")
LPup = _("Popup")
LPuc = _("Popup (cloudy on)")
LRfs = _("Refresh now")		# button
LPlw = _("Please wait...")		# button
LUpd = _("Last updated") + ": "
LErr = _("Error refreshing data")
# ===============================================================
# CONFIGURATION PAGE
# ===============================================================
class Config(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		global globset
		globset = settings
		self.settings = settings
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		bopt = ["apply_startup", "apply_asynchronously", "apply_cycle" \
		, "apply_every", "save_every", "update_scroll"]
		bscr = ["scroll_step_bright", "scroll_step_gamma", "scroll_step_color"]
		lbl1 = Gtk.Label(LApy, halign=Gtk.Align.START \
		, valign=Gtk.Align.CENTER, margin = 2)
		lbl3 = Gtk.Label(LSec, halign=Gtk.Align.START \
		, valign=Gtk.Align.CENTER, margin = 0)
		btn1 = Gtk.ToggleButton(label=LAps)
		btn1.set_tooltip_text(self.settings.get_property(bopt[0], "tooltip"))
		self.settings.bind(bopt[0], btn1, 'active', Gio.SettingsBindFlags.DEFAULT)
		btn2 = Gtk.ToggleButton(label=LApa)
		btn2.set_tooltip_text(self.settings.get_property(bopt[1], "tooltip"))
		self.settings.bind(bopt[1], btn2, 'active', Gio.SettingsBindFlags.DEFAULT)
		btn3 = Gtk.ToggleButton(label=LApe)
		btn3.set_tooltip_text(self.settings.get_property(bopt[2], "tooltip"))
		min = self.settings.get_property(bopt[3], "min")
		max = self.settings.get_property(bopt[3], "max")
		step = self.settings.get_property(bopt[3], "step")
		ttip = self.settings.get_property(bopt[3], "tooltip")
		val = self.settings.get_value(bopt[3])
		sp = Gtk.SpinButton.new_with_range(min, max, step)
		sp.set_wrap(True)
		sp.set_value(val)
		sp.set_tooltip_text(ttip)
		self.settings.bind(bopt[3], sp, 'value', Gio.SettingsBindFlags.DEFAULT)
		btn3.connect('toggled', self.cycle, sp, lbl3)
		# widget, expand, fill, padding
		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin = 0 \
		, column_spacing = 3, row_spacing = 3)
		self.pack_start(self.grid, False, True, 0)
		self.grid.add(lbl1)
		self.grid.attach(btn1, 0, 1, 1, 1)
		self.grid.attach(btn2, 2, 1, 1, 1)
		self.grid.attach(btn3, 3, 1, 1, 1)
		self.grid.attach(sp, 4, 1, 1, 1)
		self.grid.attach(lbl3, 5, 1, 1, 1)

		ck = Gtk.CheckButton.new_with_label(LIsc)
		ck.set_direction(Gtk.TextDirection.RTL)
		ck.set_alignment(0, 0)
		ck.set_halign(Gtk.Align.END)
		ck.set_valign(Gtk.Align.CENTER)
		ck.set_mode(False)		# True=checkbox, False=toggle button
		self.settings.bind("update_scroll", ck, 'active', Gio.SettingsBindFlags.DEFAULT)
		ck.connect('toggled', self.scroll)
		ck.set_tooltip_text(self.settings.get_property("update_scroll", "tooltip"))
		seph = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
		sepv = Gtk.Separator.new(Gtk.Orientation.VERTICAL)

		values = ["scroll_step_bright", "scroll_step_color", "scroll_step_gamma"]
		self.l1 = Gtk.Label(LBri, margin=5, xalign=0)
		self.l2 = Gtk.Label(LClr, margin=5, xalign=0)
		self.l3 = Gtk.Label(LGam, margin=5, xalign=0)

		self.grid.attach(seph, 0, 2, 6, 1)
		self.grid.attach(ck, 0, 3, 1, 3)
		self.grid.attach(sepv, 1, 3, 1, 3)
		self.grid.attach(self.l1, 2, 3, 2, 1)
		self.grid.attach(self.l2, 2, 4, 2, 1)
		self.grid.attach(self.l3, 2, 5, 2, 1)
		for i in values:
			defa = self.settings.get_property(i, "default")
			min = self.settings.get_property(i, "min")
			max = self.settings.get_property(i, "max")
			step = self.settings.get_property(i, "step")
			unit = self.settings.get_property(i, "units")
			tip = self.settings.get_property(i, "tooltip")
			val = self.settings.get_value(i)
			idx = values.index(i) + 1
			a = "spc" + str(idx)
			globals()[a] = Gtk.SpinButton.new_with_range(min, max, step)
			globals()[a].set_digits(0)
			globals()[a].set_increments(step, 10*step)
			globals()[a].set_value(val)
			globals()[a].set_tooltip_text(LScr + "\n\n" + tip)
			self.settings.bind(i, globals()[a], 'value', Gio.SettingsBindFlags.DEFAULT)
			k = values.index(i) + 3
			self.grid.attach(globals()[a], 4, k, 1, 1)
			a = "spcl" + str(idx)
			globals()[a] = Gtk.Label(unit, margin=2, xalign=0.5)
			self.grid.attach(globals()[a], 5, k, 1, 1)
		self.cycle(btn3, sp, lbl3)
		self.scroll(ck)
# ===============================================================
	def cycle(self, btn, w1, w2):
		isbtn = btn.get_active()
		if isbtn == True:
			w1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			w2.set_state_flags(Gtk.StateFlags.NORMAL, True)
		else:
			w1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			w2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
# ===============================================================
	def scroll(self, chk):
		ischk = chk.get_active()
		if ischk == True:
			self.l1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.l2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			self.l3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spc1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spc2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spc3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spcl1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spcl2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			spcl3.set_state_flags(Gtk.StateFlags.NORMAL, True)
		else:
			self.l1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.l2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			self.l3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spc1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spc2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spc3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spcl1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spcl2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			spcl3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
# ===============================================================
class Values(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2
		self.timer = None

		values = ["minimum_brightness", "maximum_brightness" \
		, "minimum_gamma", "maximum_gamma"]
		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin=0 \
		, column_spacing=5, row_spacing=3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.add(Gtk.Label(LOpt, margin=5, xalign=0 \
		, use_markup=True))
		self.grid.attach(Gtk.Label(LMin, margin=5, xalign=0 \
		, use_markup=True), 1, 0, 1, 1)
		self.grid.attach(Gtk.Label(LMax, margin=5, xalign=0 \
		, use_markup=True), 2, 0, 1, 1)
		self.grid.attach(Gtk.Label(LBri, margin=5, xalign=0), 0, 1, 1, 1)
		self.grid.attach(Gtk.Label(LGam, margin=5, xalign=0), 0, 2, 1, 1)

		for i in values:
			defa = self.settings.get_property(i, "default")
			min = self.settings.get_property(i, "min")
			max = self.settings.get_property(i, "max")
			step = self.settings.get_property(i, "step")
			unit = self.settings.get_property(i, "units")
			tip = self.settings.get_property(i, "tooltip")
			val = self.settings.get_value(i)
			sp = Gtk.SpinButton.new_with_range(min, max, step)
			sp.set_digits(0)
			sp.set_increments(step, 10*step)
			sp.set_value(val)
			sp.set_tooltip_text(tip)
			self.settings.bind(i, sp, "value", Gio.SettingsBindFlags.DEFAULT)
			k = values.index(i) + 1
			m = 2 - (k - 2*(k//2))
			n = (k+1) // 2
			self.grid.attach(sp, m, n, 1, 1)
			if k == 1 or k == 3:
				self.grid.attach(Gtk.Label(unit, margin=2, xalign=0.5 \
				, use_markup=False), 3, n, 1, 1)
		self.grid.insert_row(2)
		self.grid.attach(Gtk.Label(LClr, margin=5, xalign=0), 0, 2, 1, 1)
		self.grid.attach(Gtk.Label("3 000", margin=5, xalign=0), 1, 2, 1, 1)
		self.grid.attach(Gtk.Label("10 000", margin=5, xalign=0), 2, 2, 1, 1)
		self.grid.attach(Gtk.Label("K", margin=5, xalign=0), 3, 2, 1, 1)
# ===============================================================
# MODES PAGE
# ===============================================================
class Defaults(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		values = ["brightness_day", "brightness_night", "colortemp_day" \
		, "colortemp_night", "gamma_day", "gamma_night"]
		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin=0 \
		, column_spacing=5, row_spacing=3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.add(Gtk.Label(LOpt, margin=5, xalign=0 \
		, use_markup=True))
		self.grid.attach(Gtk.Label(LDay, margin=5, xalign=0 \
		, use_markup=True), 1, 0, 1, 1)
		self.grid.attach(Gtk.Label(LNgt, margin=5, xalign=0 \
		, use_markup=True), 2, 0, 1, 1)
		self.grid.attach(Gtk.Label(LBri, margin=5, xalign=0), 0, 1, 1, 1)
		self.grid.attach(Gtk.Label(LClr, margin=5, xalign=0) \
		, 0, 2, 1, 1)
		self.grid.attach(Gtk.Label(LGam, margin=5, xalign=0), 0, 3, 1, 1)

		for i in values:
			defa = self.settings.get_property(i, "default")
			min = self.settings.get_property(i, "min")
			max = self.settings.get_property(i, "max")
			step = self.settings.get_property(i, "step")
			unit = self.settings.get_property(i, "units")
			tip = self.settings.get_property(i, "tooltip")
			val = self.settings.get_value(i)
			sp = Gtk.SpinButton.new_with_range(min, max, step)
			sp.set_digits(0)
			sp.set_increments(step, 10*step)
			sp.set_value(val)
			sp.set_tooltip_text(tip)
			self.settings.bind(i, sp, 'value', Gio.SettingsBindFlags.DEFAULT)
			k = values.index(i) + 1
			m = 2 - (k - 2*(k//2))
			n = (k+1) // 2
			self.grid.attach(sp, m, n, 1, 1)
			if k == 1 or k == 3 or k == 5:
				self.grid.attach(Gtk.Label(unit, margin=5, xalign=0.5 \
				, use_markup=False), 3, n, 1, 1)
# ===============================================================
class Auto(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		global ck, t1, t2
		values = ["shift_day", "shift_night", "auto_day", "auto_night"]
		l1 = Gtk.Label(LAdj, margin=5, xalign=0)
		s0 = Gtk.Label(LOpt, margin=5, xalign=0, use_markup=True)
		s1 = Gtk.Label(LSnr, margin=5, xalign=0, use_markup=True)
		s2 = Gtk.Label(LSns, margin=5, xalign=0, use_markup=True)
		t1 = Gtk.Label(LDly, margin=5, xalign=0)
		t2 = Gtk.Label(LTim, margin=5, xalign=0)

		cb = Gtk.ComboBoxText(margin = 0)
		cb.set_entry_text_column(0)
		cb.append_text(LDis)
		cb.append_text(LOff + "  " + chr(0x1F3E1))
		cb.append_text(LOnl + "  " + chr(0x1F310))
		self.settings.bind("auto_mode", cb, 'active', Gio.SettingsBindFlags.DEFAULT)
		cb.connect('changed', self.automode)
		tip = self.settings.get_property("auto_mode", "tooltip")
		cb.set_tooltip_text(tip)

		ck = Gtk.CheckButton.new_with_label(LAlw)
		ck.set_direction(Gtk.TextDirection.RTL)
		ck.set_alignment(0, 0.5)
		ck.set_halign(Gtk.Align.END)
		ck.set_mode(False)		# True=checkbox, False=toggle button
		self.settings.bind("auto_start", ck, 'active', Gio.SettingsBindFlags.DEFAULT)
		tip = self.settings.get_property("auto_start", "tooltip")
		ck.set_tooltip_text(tip)
		sep = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)

		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin=0 \
		, column_spacing=5, row_spacing=3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.add(l1)
		self.grid.attach(cb, 1, 0, 1, 1)
		self.grid.attach(ck, 2, 0, 2, 1)
		self.grid.attach(sep, 0, 1, 4, 1)
		self.grid.attach(s0, 0, 2, 1, 1)
		self.grid.attach(s1, 1, 2, 1, 1)
		self.grid.attach(s2, 2, 2, 1, 1)
		self.grid.attach(t1, 0, 3, 1, 1)
		self.grid.attach(t2, 0, 4, 1, 1)
		for i in values:
			defa = self.settings.get_property(i, "default")
			min = self.settings.get_property(i, "min")
			max = self.settings.get_property(i, "max")
			step = self.settings.get_property(i, "step")
			unit = self.settings.get_property(i, "units")
			tip = self.settings.get_property(i, "tooltip")
			val = self.settings.get_value(i)
			k = values.index(i) + 1
			a = "sp" + str(k)
			globals()[a] = Gtk.SpinButton.new_with_range(min, max, step)
			globals()[a].set_digits(0)
			globals()[a].set_increments(step, 10*step)
			globals()[a].set_value(val)
			globals()[a].set_tooltip_text(tip)
			self.settings.bind(i, globals()[a], 'value', Gio.SettingsBindFlags.DEFAULT)
			m = 2 - (k - 2*(k//2))
			n = ((k+1) // 2) + 2
			self.grid.attach(globals()[a], m, n, 1, 1)
			if k == 1 or k == 3:
				a = "u" + str(n-2)
				globals()[a] = Gtk.Label(unit, margin=2, xalign=0, use_markup=False)
				self.grid.attach(globals()[a], 3, n, 1, 1)
		self.automode(cb)
# ===============================================================
	def automode(self, widget):
		row = widget.get_active()
		if row == 2:
			ck.set_state_flags(Gtk.StateFlags.NORMAL, True)
			t1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			t2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			u1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			u2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		elif row == 1:
			ck.set_state_flags(Gtk.StateFlags.NORMAL, True)
			t1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			t2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			u1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			u2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp1.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp2.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp3.set_state_flags(Gtk.StateFlags.NORMAL, True)
			sp4.set_state_flags(Gtk.StateFlags.NORMAL, True)
		elif row == 0:
			ck.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			t1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			t2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			u1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			u2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp1.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp2.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp3.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
			sp4.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
# ===============================================================
class Cloudy(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		ck = Gtk.CheckButton.new_with_label(LEna)
		ck.set_direction(Gtk.TextDirection.RTL)
		ck.set_alignment(0, 0)
		ck.set_halign(Gtk.Align.END)
		ck.set_valign(Gtk.Align.CENTER)
		ck.set_mode(False)		# True=checkbox, False=toggle button
		self.settings.bind("cloudy_on", ck, 'active', Gio.SettingsBindFlags.DEFAULT)
		ck.connect('toggled', self.cloudy)
		tip = self.settings.get_property("cloudy_on", "tooltip")
		ck.set_tooltip_text(tip)
		self.pack_start(ck, False, False, 0)
		sep = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
		self.pack_start(sep, False, True, 0)

		values = ["cloudy_dark", "cloudy_color", "cloudy_gamma"]
		l1 = Gtk.Label(LBri, margin=5, xalign=0)
		l2 = Gtk.Label(LClr, margin=5, xalign=0)
		l3 = Gtk.Label(LGam, margin=5, xalign=0)
		l4 = Gtk.Label("\u2013", margin=1, xalign=0.5)
		l5 = Gtk.Label("\u2013", margin=1, xalign=0.5)
		l6 = Gtk.Label("\u2013", margin=1, xalign=0.5)
		l4.margin_start = 10
		l5.margin_start = 10
		l6.margin_start = 10

		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin=0 \
		, column_spacing=2, row_spacing=3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.add(l1)
		self.grid.attach(l2, 0, 1, 1, 1)
		self.grid.attach(l3, 0, 2, 1, 1)
		self.grid.attach(l4, 1, 0, 1, 1)
		self.grid.attach(l5, 1, 1, 1, 1)
		self.grid.attach(l6, 1, 2, 1, 1)
		for i in values:
			defa = self.settings.get_property(i, "default")
			min = self.settings.get_property(i, "min")
			max = self.settings.get_property(i, "max")
			step = self.settings.get_property(i, "step")
			unit = self.settings.get_property(i, "units")
			tip = self.settings.get_property(i, "tooltip")
			val = self.settings.get_value(i)
			sp = Gtk.SpinButton.new_with_range(min, max, step)
			sp.set_digits(0)
			sp.set_increments(step, 10*step)
			sp.set_value(val)
			sp.set_tooltip_text(tip)
			self.settings.bind(i, sp, 'value', Gio.SettingsBindFlags.DEFAULT)
			k = values.index(i)
			m = 2 - (k - 2*(k//2))
			n = ((k+1) // 2)
			self.grid.attach(sp, 2, k, 1, 1)
			self.grid.attach(Gtk.Label(unit, margin=2, xalign=0.5), 3, k, 1, 1)
		self.cloudy(ck)
# ===============================================================
	def cloudy(self, chk):
		ischk = chk.get_active()
		if ischk == False:
			self.grid.set_state_flags(Gtk.StateFlags.INSENSITIVE, True)
		else:
			self.grid.set_state_flags(Gtk.StateFlags.NORMAL, True)
# ===============================================================
# GUI PAGE
# ===============================================================
class SliderButtons(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		iname = ["c_bright", "c_color", "c_gamma", "c_rgb"]
		bopt = ["menu_bright", "menu_color", "menu_gamma", "menu_rgb"]
		self.gBtns = 0
		self.clicked = False

		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin = 0 \
		, column_spacing = 5, row_spacing = 3)
		self.pack_start(self.grid, False, False, 0)

		for i in iname:
			j = iname.index(i)
			img = Gtk.Image.new_from_icon_name(i, Gtk.IconSize.LARGE_TOOLBAR)
			button = Gtk.ToggleButton()
			button.set_image(img)
			tip = self.settings.get_property(bopt[j], "tooltip")
			button.set_tooltip_text(tip)
			if j < 4:
				self.settings.bind(bopt[j], button, 'active', Gio.SettingsBindFlags.DEFAULT)
				button.connect('toggled', self.button_pressed, "buttons")
				self.gBtns += 1
			else:
				button.connect('clicked', self.click, "button_sel")
				button.set_state(Gtk.StateType.INSENSITIVE) # sets button as 'disabled'
			# grid.attach => child, left col, top row, width (cols), height (rows)
			self.grid.add(button) if j == 0 else self.grid.attach(button, j, 0, 1, 1)	
			
		cb = Gtk.ComboBoxText(margin = 0)
		self.pack_end(cb, False, True, 0)
		cb.set_entry_text_column(0)
		cb.append_text(LNon)
		cb.append_text(LSel)
		cb.append_text(LAll)
		cb.connect('changed', self.combo, "options_type", "buttons")
#		self.settings.bind('options_type', cb, 'active', Gio.SettingsBindFlags.DEFAULT)
		cb.set_tooltip_text(self.settings.get_property("options_type", "tooltip"))
		self.grid.attach(cb, j+1, 0, 1, 1)	
		o = self.settings.get_value("options_type")
		o = int(o) if o is not None else int(1)
		self.cb = cb
#		self.dbg = Gtk.Entry()
#		self.pack_start(self.dbg, True, True, 0)
		cb.set_active(o) # do it here else the dbg is missing if activating cb earlier
# ===============================================================
	def click(self, btn, key):
		a = btn.get_active()
		self.settings.set_value(key, a)
# ===============================================================
	def button_pressed(self, btn, key):
		a = btn.get_active()
		b = self.get_states()
		c = self.settings.get_value(key)
		d = self.settings.get_value('options_type')
		if int(d) == 1 and self.clicked == False:
			if str(b) == "0000": self.cb.set_active(0)
			elif str(b) == "1111": self.cb.set_active(2)
			else: self.settings.set_value(key, b)
		elif str(b) != "0000" and str(b) != "1111" and int(d) != 1 and self.clicked == False:
			btn.set_active(a)
			self.settings.set_value(key, b)
			self.cb.set_active(1)
# ===============================================================
	def get_states(self):
		b = ""
		for i in range(self.gBtns):
			r = self.grid.get_child_at(i, 0)
			s = '1' if r.get_active() == True else '0'
			b += s
		return b
# ===============================================================
	def set_states(self, key):
		b = self.settings.get_value(key)
		if b is None or b == "": b = self.settings.get_default_value(key)
		for i in range(self.gBtns):
			r = self.grid.get_child_at(i, 0)
			r.set_active(True) if b[i] == '1' else r.set_active(False)
# ===============================================================
	def combo(self, widget, own, key):
		self.clicked = True
		last = self.settings.get_value(own)
		row = widget.get_active()
		self.settings.set_value(own, row)
#		self.dbg.set_text("last=" + str(last) + ", current=" + str(row))
		if row != 1:
			if int(last) == 1:
				b = self.get_states()
				self.settings.set_value(key, b)
			for i in range(self.gBtns):
				r = self.grid.get_child_at(i, 0)
				r.set_active(True) if row == 2 else r.set_active(False)
		elif row == 1 and int(last) != 1: self.set_states(key)	
		self.clicked = False
# ===============================================================
class Extra(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		bicons = ["e_debug", "e_notif", "e_tip", "e_menu", "e_color", "e_gicon"]
		bopt = ["debug", "popup", "tooltip", "boldIt", "decor", "glyph"]
		self.grid = Gtk.Grid(halign=Gtk.Align.START, margin = 0 \
		, column_spacing = 5, row_spacing = 3)
		# widget, expand, fill, padding
		self.pack_start(self.grid, False, False, 0)
		for i in bopt:
			j = bopt.index(i)
			btn = Gtk.ToggleButton(margin=0)
			img = Gtk.Image.new_from_icon_name(bicons[j], Gtk.IconSize.LARGE_TOOLBAR)
			btn.set_image(img)
			btn.set_always_show_image(True)
			desc = self.settings.get_property(i, "description")
			btn.set_tooltip_text(desc + "\n\n" + self.settings.get_property(i, "tooltip"))
			self.settings.bind(i, btn, 'active', Gio.SettingsBindFlags.DEFAULT)
			self.grid.add(btn) if j == 0 else self.grid.attach(btn, j, 0, 1, 1)

		cb = Gtk.ComboBoxText(margin = 0)
		self.pack_end(cb, False, True, 0)
		cb.set_entry_text_column(0)
		isize = self.settings.get_property("iSz", "options")
		for i in isize:
			cb.append_text(i)
		cb.set_tooltip_text(self.settings.get_property("iSz", "tooltip"))
		o = self.settings.get_value("iSz")
		o = int(o) if o is not None else int(0)
		cb.set_active(o)
		self.settings.bind("iSz", cb, 'active', Gio.SettingsBindFlags.DEFAULT)
		self.grid.attach(cb, 6, 0, 1, 1)
# ===============================================================
class IconList(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(0)
		self.set_spacing(0)
		self.props.margin = 0

		hLabels = [LIcs, LDay, LNgt, LCst, LAut]
		rLabels = [LMds, LPup, LMdc, LPuc]
		rMode = ["sunD", "sunN", "sunC", "sunA"]
#		rModeC = []
		rPop = ["stock_weather-sunny", "stock_weather-night-clear" \
		, "stock_weather-fog", "stock_weather-showers"]
		rPopC = ["stock_weather-few-clouds", "stock_weather-night-few-clouds" \
		, "stock_weather-cloudy", "stock_weather-storm"]
		tip = self.settings.get_property("icon_list", "tooltip")
		self.grid = Gtk.Grid(halign=Gtk.Align.CENTER, margin=0, column_spacing=3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.set_tooltip_text(tip)
		for i in hLabels:
			k = hLabels.index(i)
			x = 0 if k == 0 else 0.5
			label = Gtk.Label(i, margin=5, use_markup=True, xalign=x)
			if k == 0: label.xalign = 0
			self.grid.add(label) if k == 0 else self.grid.attach(label, k, 0, 1, 1)
		for i in rLabels:
			k = rLabels.index(i)
			self.grid.attach(Gtk.Label(i, margin=5, xalign=0), 0, k+1, 1, 1)
			if k== 0 or k == 2:
				for j in rMode:
					button = Gtk.Button.new_from_icon_name(j, Gtk.IconSize.LARGE_TOOLBAR)
					self.grid.attach(button, rMode.index(j)+1, k+1, 1, 1)
					button.set_always_show_image(True)
					button.get_style_context().add_class("flat")
			else:
				iset = rPop.copy() if k == 1 else rPopC.copy()
				for j in iset:
					button = Gtk.Button.new_from_icon_name(j, Gtk.IconSize.LARGE_TOOLBAR)
					self.grid.attach(button, iset.index(j)+1, k+1, 1, 1)
					button.set_always_show_image(True)
					button.get_style_context().add_class("flat")
# ===============================================================
# INFO PAGE
# ===============================================================
class Info(SettingsWidget):
# ===============================================================
	def __init__(self, info, key, settings):
		SettingsWidget.__init__(self)
		self.settings = globset
		self.set_border_width(2)
		self.set_spacing(5)
		self.props.margin = 2

		self.override_background_color(Gtk.StateType.NORMAL \
		, Gdk.RGBA(0.83, 0.25, 0.83, 0.1))
		i = self.settings.get_value("loclast")
		lbl = Gtk.Label(LUpd + i, valign=Gtk.Align.CENTER, margin=2)
		ttip = self.settings.get_property("locupdate", "tooltip")
		btn = Gtk.ToggleButton(label=chr(0x1F5D8) + " " + LRfs)
# won't work with "\u{1F5D8}", "\u1F5D8", "\u0001F5D8", "\uD83D\uDDD8"
#		btn2 = Gtk.Button(label=chr(0x1F5D8))
		btn.set_tooltip_text(ttip)
		self.settings.bind('locupdate', btn, 'active', Gio.SettingsBindFlags.DEFAULT)
		sep = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
		sepv = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
		bfr = Gtk.TextBuffer.new()
		bfr.set_text(self.settings.get_value("locinfo"), -1)
		self.settings.bind("locinfo", bfr, 'text', Gio.SettingsBindFlags.DEFAULT)
		txt = Gtk.TextView.new_with_buffer(bfr)
		bfr.connect("modified-changed", self.locup, bfr, txt, "locinfo", lbl)
		btn.connect("toggled", self.loco, txt, lbl)
#		btn2.connect("clicked", self.locup, txt, "locinfo", lbl) # activate, clicked, released
		txt.override_background_color(Gtk.StateType.NORMAL \
		, Gdk.RGBA(0.1, 0.1, 0.1, 0.0))
		txt.set_editable(False)
		txt.set_wrap_mode(Gtk.WrapMode.WORD)
		txt.set_border_width(1)
		self.grid = Gtk.Grid(halign=Gtk.Align.CENTER, margin = 1 \
		, column_spacing = 3, row_spacing = 3)
		self.pack_start(self.grid, True, True, 0)
		self.grid.expand = True
		self.grid.add(lbl)
		self.grid.attach(sepv, 1, 0, 1, 1)
		self.grid.attach(btn, 2, 0, 1, 1)
#		self.grid.attach(btn2, 3, 0, 1, 1)
		self.grid.attach(sep, 0, 1, 4, 1)
		self.grid.attach(txt, 0, 2, 4, 1)
		self.connect("destroy", Gtk.main_quit)
# ===============================================================
	def locup(self, btn, txt, key, lbl):
		lbl.set_text("")
		i = self.settings.get_value("loclast")
		lbl.set_text(LUpd + i)
		i = self.settings.get_value("locinfo")
		if i == "":
			lbl.set_text(LErr)
			return False
		b = txt.get_buffer()
		b.set_text(i)
		txt.set_editable(False)
		return False
# ===============================================================
	def loco(self, btn, txt, lbl):
		if btn.get_active() == False:
			btn.set_label(chr(0x1F5D8) + " " + LRfs)
			GLib.timeout_add(10, self.locup, btn, txt, "locinfo", lbl)
		else:
			btn.set_label(LPlw)
			self.settings.set_value("locupdate", True)
# ===============================================================
# END SETTINGS
# ===============================================================
