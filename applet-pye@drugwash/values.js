/* THIS FILE IS PART OF THE 'APPLET PYE' APPLET FOR CINNAMON */
/* based on the original version for Brightness & Gamma applet by cardsurf */

LastValuesRow.ScreenNameColumnIndex = 0;
LastValuesRow.OutputIndexesStringColumnIndex = 1;
LastValuesRow.BrightnessColumnIndex = 2;
LastValuesRow.GammaRedColumnIndex = 3;
LastValuesRow.GammaGreenColumnIndex = 4;
LastValuesRow.GammaBlueColumnIndex = 5;
LastValuesRow.ColorTempColumnIndex = 6;
LastValuesRow.ScreenModeColumnIndex = 7;
LastValuesRow.BrightnessCustomColumnIndex = 8;
LastValuesRow.GammaColumnIndex = 9;
LastValuesRow.CsvSeparator = ",";

function LastValuesRow(screen_name, output_indexes_string, brightness,
						gamma_red, gamma_green, gamma_blue, colortemp_custom,
						screen_mode, brightness_custom, gamma) {
	this._init(screen_name, output_indexes_string, brightness,
				gamma_red, gamma_green, gamma_blue, colortemp_custom,
				screen_mode, brightness_custom, gamma);
};

LastValuesRow.prototype = {

	_init: function(screen_name, output_indexes_string, brightness,
						gamma_red, gamma_green, gamma_blue, colortemp_custom,
						screen_mode, brightness_custom, gamma) {
		this.screen_name = screen_name;
		this.output_indexes_string = output_indexes_string;
		this.brightness = brightness;
		this.gamma_red = gamma_red;
		this.gamma_green = gamma_green;
		this.gamma_blue = gamma_blue;
		this.colortemp_custom = colortemp_custom;
		this.screen_mode = screen_mode;
		this.brightness_custom = brightness_custom;
		this.gamma = gamma;
	},
};

function to_last_values_row(string) {
	let values = string.split(LastValuesRow.CsvSeparator);
	let screen_name = values[LastValuesRow.ScreenNameColumnIndex];
	let output_indexes_string = values[LastValuesRow.OutputIndexesStringColumnIndex];
	let brightness = parseInt(values[LastValuesRow.BrightnessColumnIndex]);
	let gamma_red = parseInt(values[LastValuesRow.GammaRedColumnIndex]);
	let gamma_green = parseInt(values[LastValuesRow.GammaGreenColumnIndex]);
	let gamma_blue = parseInt(values[LastValuesRow.GammaBlueColumnIndex]);
	let colortemp_custom = parseInt(values[LastValuesRow.ColorTempColumnIndex]);
	let gamma = parseInt(values[LastValuesRow.GammaColumnIndex]);
	let screen_mode = parseInt(values[LastValuesRow.ScreenModeColumnIndex]);
	let brightness_custom = parseInt(values[LastValuesRow.BrightnessCustomColumnIndex]);
	let row = new LastValuesRow(screen_name, output_indexes_string, brightness,
				gamma_red, gamma_green, gamma_blue, colortemp_custom,
				screen_mode, brightness_custom, gamma);
	return row;
};

function to_csv_string (row) {
	let string_csv = row.screen_name + LastValuesRow.CsvSeparator;
	string_csv += row.output_indexes_string + LastValuesRow.CsvSeparator;
	string_csv += row.brightness + LastValuesRow.CsvSeparator;
	string_csv += row.gamma_red + LastValuesRow.CsvSeparator;
	string_csv += row.gamma_green + LastValuesRow.CsvSeparator;
	string_csv += row.gamma_blue + LastValuesRow.CsvSeparator;
	string_csv += row.colortemp_custom + LastValuesRow.CsvSeparator;
	string_csv += row.screen_mode + LastValuesRow.CsvSeparator;
	string_csv += row.brightness_custom + LastValuesRow.CsvSeparator;
	string_csv += row.gamma;
// global.log("Applet PYE: csv row=" + string_csv);
	return string_csv;
};
