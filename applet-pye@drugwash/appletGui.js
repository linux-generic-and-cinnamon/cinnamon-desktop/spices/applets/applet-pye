/* THIS FILE IS PART OF THE 'APPLET PYE' APPLET FOR CINNAMON */
/* based on the original version for Brightness & Gamma applet by cardsurf */

const Lang = imports.lang;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const St = imports.gi.St;
const Applet = imports.ui.applet;
const Clutter = imports.gi.Clutter;
const PopupMenu = imports.ui.popupMenu;
const Tooltips = imports.ui.tooltips;
const Gettext = imports.gettext;
const Params = imports.misc.params;
const uuid = "applet-pye@drugwash";
const MS = "MenuSliders > ";
let CtrlPlus;
if (typeof require !== 'undefined')
	CtrlPlus = require('./ctrlPlus');
else {
	const AppletDirectory = imports.ui.appletManager.applets[uuid];
	CtrlPlus = AppletDirectory.ctrlPlus;
}

// Translation support
Gettext.bindtextdomain(uuid, GLib.get_home_dir() + "/.local/share/locale")
/*=============================================================*/
function _(str) {
  return Gettext.dgettext(uuid, str);
}
/*========================== RADIO ============================*/
function RadioMenuItem(applet, title, option_names) {
	this._init(applet, title, option_names);
};

RadioMenuItem.prototype = {
	__proto__: PopupMenu.PopupSubMenuMenuItem.prototype,

	_init: function(applet, title, option_names) {
		PopupMenu.PopupSubMenuMenuItem.prototype._init.call(this, title, false);
		this.applet = applet;
		this.options = [];
		this.dictionary_option_name_index = {};
		this.active_option_index = -1;
		this.callback_object = null;
		this.callback_option_clicked = null;
		this._init_options(option_names);
	},

	_init_options: function(option_names) {
		for (let option_name of option_names) {
			let option = new PopupMenu.PopupMenuItem(option_name, false);
			option.label.set_style("font-size: " + this.applet.fSize + ";");
			option.connect('activate', Lang.bind(this, this._on_option_clicked));
			this.menu.addMenuItem(option);
			this.options.push(option);
			this.dictionary_option_name_index[option_name] = this.options.length - 1;
		}
	},

	_on_option_clicked: function (option, event) {
		let index_clicked = this.options.indexOf(option);
		this.set_active_option_index(index_clicked);
		this._invoke_callback_option_clicked();
	},

	set_active_option_name: function(option_name) {
		let valid = this.is_option_name_valid(option_name);
		if (valid) {
			let index = this.dictionary_option_name_index[option_name];
			this.set_active_option_index(index);
		}
	},

	is_option_name_valid: function(option_name) {
		return this.dictionary_contains(this.dictionary_option_name_index, option_name);
	},

	dictionary_contains: function (dictionary, key) {
		return key in dictionary;
	},

	set_active_option_index: function(index) {
		if (this.is_option_index_valid(index))
			this.set_active_option_index_valid(index);
	},

	is_option_index_valid: function(index) {
		return index >= 0 && index < this.options.length;
	},

	set_active_option_index_valid: function(index) {
		if (this.active_option_index != index) {
			if (this.active_option_index != -1) {
				this.set_font_weight(this.active_option_index, "normal");
			}
			this.set_font_weight(index, "bold");
			this.active_option_index = index;
		}
	},

	set_font_weight: function(option_index, font_weight) {
		let css_style = "font-size: " + this.applet.fSize + "; font-weight: " + font_weight + ";";
		let option = this.options[option_index];
		this.set_option_style(option, css_style);
	},

	set_option_style: function(option, css_style) {
		option.label.set_style(css_style);
	},

	_invoke_callback_option_clicked: function() {
		if (this.callback_option_clicked != null) {
			let option_name = this.get_active_option_name();
			this.callback_option_clicked.call(this.callback_object, option_name, this.active_option_index);
		}
	},

	get_active_option_name: function() {
		let option = this.get_active_option();
		let option_name = this.get_option_name(option);
		return option_name;
	},

	get_active_option: function() {
		return this.options[this.active_option_index];
	},

	get_option_name: function(option) {
		return option.label.get_text();
	},

	get_active_option_index: function() {
		return this.active_option_index;
	},

	set_callback_option_clicked: function(callback_object, callback_option_clicked) {
		this.callback_object = callback_object;
		this.callback_option_clicked = callback_option_clicked;
	},
};
/*========================= CHECKBOX ==========================*/
function CheckboxMenuItem(applet, title) {
	this._init(applet, title);
};

CheckboxMenuItem.prototype = {
	__proto__: PopupMenu.PopupSubMenuMenuItem.prototype,

	_init: function(applet, title) {
		PopupMenu.PopupSubMenuMenuItem.prototype._init.call(this, title, false);

		this.options = [];
		this.callback_object = null;
		this.callback_option_toggled = null;
		this.applet = applet;
		this.dbg = this.applet.dbgsw;
	},

	reload_options: function(option_names, options_checked) {
		this.remove_options();
		this.applet._init_screen_outputs();
		this.create_options(option_names, options_checked);
		this.add_options(option_names);
	},

	remove_options: function() {
		 this.menu.removeAll()
		 this.options = [];
	},

	create_options: function(option_names, options_checked) {
		for (let i = 0; i < option_names.length; ++i) {
			let option_name = option_names[i];
			let option_checked = options_checked[i];
//			let option = new PopupMenu.PopupSwitchIconMenuItem(option_name, option_checked, "", this.applet.icontype);
			let option = new PopupMenu.PopupSwitchIconMenuItem(option_name, false, "", this.applet.icontype);
			option.tooltip = new Tooltips.Tooltip(option.actor,"");
			this._status(option, option_checked, i);
			option.connect('toggled', Lang.bind(this, this._on_option_toggled));
			this.options.push(option);
		}
	},

	_on_option_toggled: function (option, checked) {
		this._status(option, checked);
		this._invoke_callback_option_toggled(option, checked);
		this.applet._init_screen_outputs();
		try {
			let ttsv = this.applet.refresh_active_outputs();
			this.applet.update_tooltip_values()
			if (this.dbg) global.log(ttsv);
		} catch(e) { global.log(e); }
	},

	_status: function (item, checked, idx=null) {
		idx = (idx == null) ? this.options.indexOf(item) : idx;
		let conn = this.applet.screen_outputs[this.applet.screen_name][idx].is_connected;
		let active = this.applet.isActiveOutput(idx);
		this.applet.screen_outputs[this.applet.screen_name][idx].is_enabled = active;
		this.applet._addIcon(item, this.applet.iObj[conn ? 3 : 10], this.applet.iSize);
		item.label.set_style("font-size: " + this.applet.fSize + "; font-weight: " + (active && checked ? "bold" : "normal") + ";");
		item.tooltip.set_text(active ? "" : (conn ? this.applet.TToutDisab : this.applet.TToutDiscon));
		item.setStatus(active ? "" : (conn ? this.applet.disabled : this.applet.disconn));
		item.setToggleState(active & checked);
		return active;
	},

	_invoke_callback_option_toggled: function(option, checked) {
		if (this.callback_option_toggled != null) {
			let option_index = this.options.indexOf(option);
			let option_name = this.get_option_name(option);
			this.callback_option_toggled.call(this.callback_object, option_index, option_name, checked);
		}
	},

	get_option_name: function(option) {
		return option.label.get_text();
	},

	add_options: function(option_names) {
		for (let option of this.options) {
			this.menu.addMenuItem(option);
		}
	},

	set_callback_option_toggled: function(callback_object, callback_option_toggled) {
		this.callback_object = callback_object;
		this.callback_option_toggled = callback_option_toggled;
	},

};
/*========================== SLIDERS ==========================*/
function MenuSliders(applet, orientation){
	this._init(applet, orientation);
}

MenuSliders.prototype={

	_init: function(applet, orientation) {
		this.applet			= applet;
		this.orientation	= orientation;
		this.dbg			= this.applet.dbgsw;
		if (this.dbg) global.log("Initializing new MenuSliders...");
		this.menu			= new Applet.AppletPopupMenu(applet, this.orientation);
		this.section		= new PopupMenu.PopupMenuSection();
		this.labels			= {};
		this.sliders		= {};
		this.header_txt			= _("Available controls:");
		this.header_none		= _("Controls are disabled");
		this.header_key			= _("Header");
		this.brightness_key		= _("Brightness");
		this.colortemp_key		= _("Color temperature");
		this.gamma_key			= _("Gamma");
		this.gamma_red_key		= _("Red");
		this.gamma_green_key	= _("Green");
		this.gamma_blue_key		= _("Blue");
		this.lastMax	= this.applet.gamma;
		this.icontype	= this.applet.icontype;
		this.iconB		= this.applet.iObj[0]; //"gnome-brightness-applet";
		this.iconC		= this.applet.iObj[1]; //"preferences-color";
		this.iconG		= this.applet.iObj[2]; //"contrast";
		this.rgbw		= this.applet.ci.slice()
		this.icons		= {};
		this.icons[this.brightness_key]		= this.iconB;
		this.icons[this.colortemp_key]		= this.iconC;
		this.icons[this.gamma_key]			= this.iconG;
		this.icons[this.gamma_red_key]		= this.rgbw[12];
		this.icons[this.gamma_green_key]	= this.rgbw[13];
		this.icons[this.gamma_blue_key]		= this.rgbw[14];
		this.iSize		= this.applet.iSize;	//label icon size
		this.dBright	= 2;			// delta magnetic slider Brightness
		this.dColor		= 99;			// delta magnetic slider Colortemp
		this.dRGB		= 2;			// delta magnetic sliders Gamma and RGB
		let bStep		= 1;			// brightness step 1 unit (%)
		let cStep		= 100;			// colortemp step 100 units (K)
		let gStep		= 1;			// gamma step 1 unit (%)
		this.brightStep = bStep / (this.applet.maximum_brightness - this.applet.minimum_brightness + 1);
		this.colorStep	= cStep / (this.applet.maximum_colortemp - this.applet.minimum_colortemp + 1);
		this.gammaStep	= gStep / (this.applet.maximum_gamma - this.applet.minimum_gamma + 1);

		this._init_menu();
		this._init_items();
		this._add_menu_to_applet();
		if (this.dbg) global.log("Finished initializing new MenuSliders");
	},

	_init_menu: function(){
		this.menu.addMenuItem(this.section);
	},

	_init_items: function (){
		if (this.dbg) global.log(MS + "_init_items()");
		this._init_header_label();
		this._init_items_brightness_active();
		this._init_items_colortemp_active();
		this._init_items_gamma_active();
		this._init_items_rgb_active();
	},

	_init_header_label: function () {
		let text = (this.applet.is_brightness_active() || this.applet.is_colortemp_active() || this.applet.is_gamma_active()) ?
			this.header_txt : this.header_none;
		let s = (this.applet.decor) ? this.applet.hdrstyle : this.applet.simpleStyle;
		let bColor = new Clutter.Color({red: 255, green: 255, blue:255, alpha: 255}); // horrible hack! :-(
//		this.add_label(this.header_key, text, { style:s, bkg:bColor, iName:"folder-color-switcher-white" });
		this.add_label(this.header_key, text, { style:s, bkg:bColor, iName:this.rgbw[15] });
		this.applet._addSeparator(this.menu);
	},

	_magnet: function(src, val, delta, min, max) {
		if (src._mark_position == 0 || src._mark_position == 1) return val;
		let m = min + src._mark_position * (max - min);
		let r = (val > m + delta || val < m - delta) ? val : m;
		let s = (r - min) / (max - min);
		src.setValue(s);
		return r;
	},

	add_label: function(key, text, par){
		let label;
		if (typeof par.iName === 'undefined') label = new PopupMenu.PopupMenuItem(text, { reactive: false });
		else label = new PopupMenu.PopupIconMenuItem(text, par.iName, this.applet.icontype, { reactive: false });
		label.actor.set_style_class_name('');	// this was killing me with that extra empty space !!!
		label.removeActor(label.label);
		label.addActor(label.label, {span: -1, expand: true, align: St.Align.START});
		if (typeof par.bkg != 'undefined')
			try { label.actor.set_background_color(par.bkg); }
			catch(e) { global.log("label.actor.set_background_color() " + e) }
		label.label.set_text(text);
		if (par.style) label.label.set_style(par.style);
		else if (this.applet.boldIt) label.label.set_style("font-weight: bold;");
		this.menu.addMenuItem(label, {align: St.Align.START, x_align: St.Align.START});
		this.labels[key] = label;
	},

	set_label_text: function(key, value) {
		let label = this.labels[key];
		let text = this.get_description_text(key, value);
		label.label.set_text(text);
	},

	get_description_text: function(key, value) {
		let unit = (key == this.colortemp_key) ? " K" : "%";
		return key + ": " + value + unit;
	},

	_init_slider: function (key, callback, min_value, max_value, value) {
		if (this.dbg) global.log(MS + "_init_slider() > " + key);
		let zero_one_range_value = this.get_zero_one_range_value(min_value, max_value, value);
		let slider = this.add_slider(key, zero_one_range_value);
		slider.label.text = value + slider.units;
		slider.tooltip.set_text(key + ": " + value + slider.units);
// this is needed when clicking in slider shaft, otherwise value is not being updated (only in official version)
//		slider.connect('drag-end', Lang.bind(this, function(){slider.emit('value-changed', slider._value);}));
		slider.connect('value-changed', Lang.bind(this, callback));
		if (key == this.brightness_key) {
			slider.set_mark(this.getMark(this.applet.minimum_brightness,
				this.applet.maximum_brightness, this.applet.default_brightness_day));	// what if THIS day value changes...?
		}
		else if (key == this.colortemp_key) {
			slider.set_mark(this.getMark(this.applet.minimum_colortemp,
				this.applet.maximum_colortemp, this.applet.default_colortemp_day));	// what if THIS day value changes...?
		}
		else if (key == this.gamma_key || key == this.gamma_red_key ||
			key == this.gamma_green_key || key == this.gamma_blue_key)
			slider.set_mark(this.getMark(this.applet.minimum_gamma,
				this.applet.maximum_gamma, this.applet.default_gamma_day));	// what if THIS day value changes...?
		slider.emit('value-changed', slider._value);
		return slider;
	},

	getMark: function (min, max, def) {
		let m = (def - min) / (max - min);
		return (m == 1 ? 0 : m);
	},

	get_zero_one_range_value: function(min_value, max_value, value) {
		value = this.get_range_value(min_value, max_value, value);
		return (value - min_value) / (max_value - min_value);
		let offset = min_value;
		let divisor = max_value - min_value;
		let mapped_value = value - offset;
		let value_zero_one_range = mapped_value / divisor;
		return value_zero_one_range;
	},

	get_range_value: function (min_value, max_value, value) {
		return Math.max(Math.min(value, max_value), min_value);
	},

	add_slider: function(key, zero_one_range_value){
		if (this.dbg) global.log(MS + "add_slider() > " + key);
		let s = Math.round(5 + Math.sqrt(this.iSize)) + "pt";
		let y = (this.applet.boldIt) ? "bold" : "normal";
		let u = (key == this.colortemp_key) ? "K" : "%";
		let d = (key == this.colortemp_key) ? this.colorStep :
			(key == this.brightness_key) ? this.brightStep : this.gammaStep; // 100/7001 : 1/91: 1/101
		let i = (this.applet.decor && key == this.colortemp_key) ? this.applet.colormap_path : "";
		let slider = new CtrlPlus.PopupSliderPlusMenuItem(this.icons[key], zero_one_range_value,
			{text: key, fSize: s, fStyle: y, units: u, iSize: this.iSize, iType: this.icontype, img: i
			, kDelta: d, sStep: d}
		);
		this.menu.addMenuItem(slider);
		this.sliders[key] = slider;
		return slider;
	},

	get_slider_value: function(min_value, max_value, value) {
		let offset = min_value;
		let multiplier = max_value - min_value;
		let value_zero_one_range = parseFloat(value);
		let mapped_value = Math.round(offset + value_zero_one_range * multiplier);
		return mapped_value;
	},
/*=========== CONDITIONAL INITIALIZATIONS ===========*/
	_init_items_brightness_active: function () {
		if (this.dbg) global.log(MS + "_init_items_brightness_active()");
		if (this.applet.is_brightness_active()) this._init_items_brightness();
	},

	_init_items_colortemp_active: function () {
		if (this.dbg) global.log(MS + "_init_items_colortemp_active()");
		if (this.applet.is_colortemp_active()) this._init_items_colortemp();
	},

	_init_items_gamma_active: function () {
		if (this.dbg) global.log(MS + "_init_items_gamma_active()");
		if (this.applet.is_gamma_active()) this._init_items_gamma();
	},

	_init_items_rgb_active: function () {
		if (this.dbg) global.log(MS + "_init_items_rgb_active()");
		if (this.applet.is_rgb_active()) this._init_items_rgb();
	},
/*=========== PRIMARY INITIALIZATIONS ===========*/
	_init_items_brightness: function(){
		if (this.dbg) global.log(MS + "_init_items_brightness()");
		this.sb = this._init_slider(this.brightness_key, this.on_brightness_slider_changed,
			this.applet.minimum_brightness, this.applet.maximum_brightness, this.applet.brightness);
	},

	_init_items_colortemp: function(){
		if (this.dbg) global.log(MS + "_init_items_colortemp()");
		this.sc = this._init_slider(this.colortemp_key, this.on_colortemp_slider_changed,
			this.applet.minimum_colortemp, this.applet.maximum_colortemp, this.applet.colortemp);
	},

	_init_items_gamma: function () {	// this holds the order of the sliders in the menu
		if (this.dbg) global.log(MS + "_init_items_gamma()");
		this.sg = this._init_slider(this.gamma_key, this.on_gamma_slider_changed,
			this.applet.minimum_gamma, this.applet.maximum_gamma, this.applet.gamma);
	},

	_init_items_rgb: function () {	// this holds the order of the sliders in the menu
		if (this.dbg) global.log(MS + "_init_items_rgb()");
		if (this.applet.is_gamma_active() ||
			this.applet.is_colortemp_active() ||
			this.applet.is_brightness_active()
		) this.applet._addSeparator(this.menu);
		this._init_items_gamma_red();
		this._init_items_gamma_green();
		this._init_items_gamma_blue();
	},

	_init_items_gamma_red: function(){
		if (this.dbg) global.log(MS + "_init_items_gamma_red()");
		this.gr = this._init_slider(this.gamma_red_key, this.on_gamma_red_slider_changed, this.applet.minimum_gamma,
							this.applet.maximum_gamma, this.applet.gamma_red);
	},

	_init_items_gamma_green: function(){
		if (this.dbg) global.log(MS + "_init_items_gamma_green()");
		this.gg = this._init_slider(this.gamma_green_key, this.on_gamma_green_slider_changed, this.applet.minimum_gamma,
							this.applet.maximum_gamma, this.applet.gamma_green);
	},

	_init_items_gamma_blue: function(){
		if (this.dbg) global.log(MS + "_init_items_gamma_blue()");
		this.gb = this._init_slider(this.gamma_blue_key, this.on_gamma_blue_slider_changed, this.applet.minimum_gamma,
							this.applet.maximum_gamma, this.applet.gamma_blue);
	},
/* =========== SLIDER CALLBACKS ===========*/
	_updateFeedback: function(src, val) {
		src.label.text = val.toString() + src.units;
		src.tooltip.set_text(src.text + ": " + val.toString()  + src.units);
//global.log("src=" + src + ", val=" + val + ", units=" + src.units + ", text=" + src.text);
	},

	on_brightness_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_brightness, this.applet.maximum_brightness, value);
		mapped_value = this._magnet(source_event, mapped_value, this.dBright,
			this.applet.minimum_brightness, this.applet.maximum_brightness);
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_brightness(mapped_value);
		this.applet.update_tooltip_values();
	},

	on_colortemp_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_colortemp, this.applet.maximum_colortemp, value);
		mapped_value = this._magnet(source_event, mapped_value, this.dColor,
			this.applet.minimum_colortemp, this.applet.maximum_colortemp);
//		this.applet.update_colortemp(mapped_value);
		if (this.dbg) global.log("CT=" + mapped_value + "|RGB=" + this.applet.gamma_red +
			", " + this.applet.gamma_green + ", " + this.applet.gamma_blue);
		this.updateCT(mapped_value, this.applet.gamma);
		if (this.dbg) global.log("Gamma=" + this.applet.gamma + "|New RGB=" + this.applet.gamma_red + ", " +
			this.applet.gamma_green + ", " + this.applet.gamma_blue);
		this.update_items_rgb();
		mapped_value = this._magnet(source_event, mapped_value, this.dColor,
			this.applet.minimum_colortemp, this.applet.maximum_colortemp);
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_colortemp(mapped_value);
		this.applet.update_tooltip_values();
	},

	on_gamma_red_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_gamma, this.applet.maximum_gamma, value);
 		mapped_value = this._checkMinMax(this.gamma_red_key, this.applet.minimum_gamma,
			this.applet.maximum_gamma, this.applet.gamma, mapped_value);
//global.log(this.gamma_red_key + ": min=" + this.applet.minimum_gamma.toString() + ", max=" + this.applet.maximum_gamma.toString() + ", cur=" + this.applet.gamma.toString() + ", map=" + mapped_value.toString());
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_gamma_red(mapped_value);
		this._fixCT();
		if (this.dbg) global.log("R=" + this.applet.gamma_red + " - CT=" + this.applet.colortemp);
		this.applet.update_tooltip_values();
	},

	on_gamma_green_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_gamma, this.applet.maximum_gamma, value);
 		mapped_value = this._checkMinMax(this.gamma_green_key, this.applet.minimum_gamma,
			this.applet.maximum_gamma, this.applet.gamma, mapped_value);
//global.log(this.gamma_green_key + ": min=" + this.applet.minimum_gamma.toString() + ", max=" + this.applet.maximum_gamma.toString() + ", cur=" + this.applet.gamma.toString() + ", map=" + mapped_value.toString());
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_gamma_green(mapped_value);
		this._fixCT();
		if (this.dbg) global.log("G=" + this.applet.gamma_green + " - CT=" + this.applet.colortemp);
		this.applet.update_tooltip_values();
	},

	on_gamma_blue_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_gamma, this.applet.maximum_gamma, value);
		mapped_value = this._checkMinMax(this.gamma_blue_key, this.applet.minimum_gamma,
			this.applet.maximum_gamma, this.applet.gamma, mapped_value);
//global.log(this.gamma_blue_key + ": min=" + this.applet.minimum_gamma.toString() + ", max=" + this.applet.maximum_gamma.toString() + ", cur=" + this.applet.gamma.toString() + ", map=" + mapped_value.toString());
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_gamma_blue(mapped_value);
		this._fixCT();
		if (this.dbg) global.log("B=" + this.applet.gamma_blue + " - CT=" + this.applet.colortemp);
		this.applet.update_tooltip_values();
	},

	on_gamma_slider_changed: function(source_event, value) {
		let mapped_value = this.get_slider_value(this.applet.minimum_gamma, this.applet.maximum_gamma, value);
		mapped_value = this._checkMinMax(this.gamma_key, this.applet.minimum_gamma,
			this.applet.maximum_gamma, this.applet.maximum_gamma, mapped_value);
		mapped_value = this._magnet(source_event, mapped_value, this.dRGB,
			this.applet.minimum_gamma, this.applet.maximum_gamma);
//global.log(this.gamma_key + ": min=" + this.applet.minimum_gamma.toString() + ", max=" + this.applet.maximum_gamma.toString() + ", cur=" + this.applet.gamma.toString() + ", map=" + mapped_value.toString() + ", dRGB=" + this.dRGB.toString());
		this._updateFeedback(source_event, mapped_value);
		this.applet.update_gamma(mapped_value);
		this.gamma2RGB(this.applet.gamma);
		this._fixCT();
//		this.lastMax = mapped_value;
		if (this.applet.is_rgb_active()) this.update_items_rgb();
		else this.applet.update_tooltip_values();
	},
/*============= VALUE CORRECTIONS =============*/
	_fixCT: function() {
		this.applet.colortemp = this.rgb2ct(this.applet.gamma_red, this.applet.gamma_green, this.applet.gamma_blue,
			this.applet.minimum_colortemp, this.applet.maximum_colortemp);
		this.applet.colortemp = Math.ceil(this.applet.colortemp / 100) * 100;	// try a 50 unit precision
		this.update_items_colortemp();
		this.updateCT(this.applet.colortemp, this.applet.gamma);
	},

	gamma2RGB: function(val, map) {
		this.applet.gamma_red = this._applyMaxGamma(this.applet.gamma_red, val);
		this.applet.gamma_green = this._applyMaxGamma(this.applet.gamma_green, val);
		this.applet.gamma_blue = this._applyMaxGamma(this.applet.gamma_blue, val);
		this.lastMax = val;
	},

	_applyMaxGamma: function (val, max) {
		if (max > this.applet.maximum_gamma) max = this.applet.maximum_gamma;
		let res = max-(this.lastMax-val);
		if (res < this.applet.minimum_gamma) return this.applet.minimum_gamma;
		if (res > max) return max;
		return res;
	},

	_checkMinMax: function (key, min, max, cap, val) {
		var tr, tg, tb, tt;
		if (val < min) val = min;
		else if (val > cap) val = cap;
		if (key == this.gamma_red_key) tr = val; else tr = this.applet.gamma_red;
		if (key == this.gamma_green_key) tg = val; else tg = this.applet.gamma_green;
		if (key == this.gamma_blue_key) tb = val; else tb = this.applet.gamma_blue;
		tt = this.rgb2ct(tr, tg, tb, this.applet.minimum_colortemp, this.applet.maximum_colortemp);
		if (tt > this.applet.maximum_colortemp) {
			this.updateCT(this.applet.maximum_colortemp, this.applet.gamma);
			if (key == this.gamma_red_key) val = this.applet.gamma_red;
			if (key == this.gamma_green_key) val = this.applet.gamma_green;
			if (key == this.gamma_blue_key) val = this.applet.gamma_blue;
//global.log(key + " val2=" + val);
		}
		else if (tt < this.applet.minimum_colortemp) {
			this.updateCT(this.applet.minimum_colortemp, this.applet.gamma);
			if (key == this.gamma_red_key) val = this.applet.gamma_red;
			if (key == this.gamma_green_key) val = this.applet.gamma_green;
			if (key == this.gamma_blue_key) val = this.applet.gamma_blue;
//global.log(key + " val2=" + val);
		}
		let r = this.get_zero_one_range_value(min, max, val);
//		this.set_slider_value(key, r);
		this.set_slider_value(key, r, val); // this should quiet the log
//global.log("out: " + "min=" + min + " max=" + max + " cap=" + cap + " val=" + val + " r=" + r);
	return val;
	},

	_add_menu_to_applet: function(){
		if (this.dbg) global.log(MS + "_add_menu_to_applet()");
		this.menuManager = new PopupMenu.PopupMenuManager(this.applet);
		this.menuManager.addMenu(this.menu);
	},
/*============ SLIDER UPDATES ============*/
	update_items_brightness: function () {
		if (this.dbg) global.log(MS + "update_items_brightness()");
		if (this.applet.is_brightness_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
			this.applet.minimum_brightness, this.applet.maximum_brightness, this.applet.brightness);
			this.set_slider_value(this.brightness_key, zero_one_range_value, this.applet.brightness);
		}
		this.applet.update_tooltip_values()
	},

	update_items_colortemp: function () {
		if (this.dbg) global.log(MS + "update_items_colortemp()");
		if (this.applet.is_colortemp_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
				this.applet.minimum_colortemp, this.applet.maximum_colortemp, this.applet.colortemp);
			this.set_slider_value(this.colortemp_key, zero_one_range_value, this.applet.colortemp);
		}
		this.applet.update_tooltip_values()
	},

	update_items_gamma: function () {
		if (this.dbg) global.log(MS + "update_items_gamma()");
		if (this.applet.is_gamma_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
				this.applet.minimum_gamma, this.applet.maximum_gamma, this.applet.gamma);
			this.set_slider_value(this.gamma_key, zero_one_range_value, this.applet.gamma);
		}
		this.applet.update_tooltip_values()
	},

	update_items_rgb: function () {
		if (this.dbg) global.log(MS + "update_items_rgb()");
		if (this.applet.is_gamma_active() && this.applet.is_rgb_active()) {
			this.update_items_gamma_red(true);
			this.update_items_gamma_green(true);
			this.update_items_gamma_blue(true);
		}
		this.applet.update_tooltip_values()
	},

	update_items_gamma_red: function (noTip) {
		if (this.dbg) global.log(MS + "update_items_gamma_red()");
		if (this.applet.is_gamma_active() && this.applet.is_rgb_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
				this.applet.minimum_gamma, this.applet.maximum_gamma, this.applet.gamma_red);
			this.set_slider_value(this.gamma_red_key, zero_one_range_value, this.applet.gamma_red);
		}
		if (!noTip) this.applet.update_tooltip_values()
	},

	update_items_gamma_green: function (noTip) {
		if (this.dbg) global.log(MS + "update_items_gamma_green()");
		if (this.applet.is_gamma_active() && this.applet.is_rgb_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
				this.applet.minimum_gamma, this.applet.maximum_gamma, this.applet.gamma_green);
			this.set_slider_value(this.gamma_green_key, zero_one_range_value, this.applet.gamma_green);
		}
		if (!noTip) this.applet.update_tooltip_values()
	},

	update_items_gamma_blue: function (noTip) {
		if (this.dbg) global.log(MS + "update_items_gamma_blue()");
		if (this.applet.is_gamma_active() && this.applet.is_rgb_active()) {
			let zero_one_range_value = this.get_zero_one_range_value(
				this.applet.minimum_gamma, this.applet.maximum_gamma, this.applet.gamma_blue);
			this.set_slider_value(this.gamma_blue_key, zero_one_range_value, this.applet.gamma_blue);
		}
		if (!noTip) this.applet.update_tooltip_values()
	},

	set_slider_value: function (key, bval, val) {
		let slider = this.sliders[key];
		slider.setValue(bval);
		try {
			if (typeof val != "number")
				global.logWarning("slider " + key.toString() +
					" does _updateFeedback() with val=" + (typeof val));
			else this._updateFeedback(slider, val);
		} catch(e) { global.logWarning("[Applet PYE] slider " + key.toString() + " undefined"); }
	},
/*============ MENU OPERATIONS ============*/
	remove: function(){
		if (this.dbg) global.log(MS + "remove()");
		this.menuManager.removeMenu(this.menu);
	},

	open: function(){
		this.menu.open();
	},

	close: function(){
		this.menu.close();
	},

	toggle: function(){
		this.menu.toggle();
	},
/*=========== RGB <=> COLORTEMP CONVERSIONS ===========*/
// from http://web.archive.org/web/20161108193229/ +
//			http://tommitytom.co.uk:80/colourtemp/ (slightly modified)
	clamp: function(value, min, max) {
//		let r = Math.round(Math.max(Math.min(value, 255), 0) * 100 / 255);
		let r = Math.round(Math.max(Math.min(value, 255), 0) * this.applet.gamma / 255);
		if (!min && !max) return r;
		return Math.max(Math.min(r, max), min);
	},

	 updateCT: function(temp, m) {
		this.ct2rgb(temp, m);
		return;
		var r = 0, g = 0, b = 0,
//			temp = Math.floor(temp / 100 + 0.5);
			temp = Math.round(temp / 100);
		m /= 100;
		if (temp <= 66) {
			r = 255;
			g = temp;
			g = 99.4708025861 * Math.log(g) - 161.1195681661;
		} else {
			r = temp - 60;
			r = 329.698727446 * Math.pow(r, -0.1332047592);
			g = temp - 60;
			g = 288.1221695283 * Math.pow(g, -0.0755148492);
		}

		if (temp >= 66) b = 255;
		else if (temp <= 19) b = 0;
		else {
			b = temp - 10;
			b = 138.5177312231 * Math.log(b) - 305.0447927307;
		}

		this.applet.gamma_red = this.clamp(r * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
		this.applet.gamma_green = this.clamp(g * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
		this.applet.gamma_blue = this.clamp(b * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
	},

// from https://github.com/neilbartlett/color-temperature (slightly modified)
	rgb2ct: function(r, g, b, minT, maxT) {
		var temperature, testRGB;
		var epsilon=0.4;
		var minTemperature = (minT != 'undefined') ? minT : 1000;
		var maxTemperature = (maxT != 'undefined') ? maxT : 40000;
		while (maxTemperature - minTemperature > epsilon) {
			temperature = (maxTemperature + minTemperature) / 2;
			testRGB = this.ct2rgb(temperature);
			if ((testRGB.blue / testRGB.red) >= (b / r))
				maxTemperature = temperature;
			else
				minTemperature = temperature;
		}
		return Math.round(temperature);
	},

	ct2rgb: function(kelvin, m) {
		var temperature = kelvin / 100.0;
		var red, green, blue;

		if (temperature < 66.0) red = 255;
		else {
			// a + b x + c Log[x] /.
			// {a -> 351.97690566805693`,
			// b -> 0.114206453784165`,
			// c -> -40.25366309332127
			//x -> (kelvin/100) - 55}
			red = temperature - 55.0;
			red = 351.97690566805693+ 0.114206453784165 * red - 40.25366309332127 * Math.log(red);
		}

		if (temperature < 66.0) {
			// a + b x + c Log[x] /.
			// {a -> -155.25485562709179`,
			// b -> -0.44596950469579133`,
			// c -> 104.49216199393888`,
			// x -> (kelvin/100) - 2}
			green = temperature - 2;
			green = -155.25485562709179 - 0.44596950469579133 * green + 104.49216199393888 * Math.log(green);
		} else {
			// a + b x + c Log[x] /.
			// {a -> 325.4494125711974`,
			// b -> 0.07943456536662342`,
			// c -> -28.0852963507957`,
			// x -> (kelvin/100) - 50}
			green = temperature - 50.0;
			green = 325.4494125711974 + 0.07943456536662342 * green - 28.0852963507957 * Math.log(green);
		}
		green += 7.0;		// just trying to get a 100/100/100 indication for 6500K
		if (temperature >= 66.0) blue = 255;
		else {
			if (temperature <= 20.0) blue = 0;	// Tanner Helland's uses 19 here
			else {
			// a + b x + c Log[x] /.
			// {a -> -254.76935184120902`,
			// b -> 0.8274096064007395`,
			// c -> 115.67994401066147`,
			// x -> kelvin/100 - 10}
			blue = temperature - 10.0;
			blue = -254.76935184120902 + 0.8274096064007395 * blue + 115.67994401066147 * Math.log(blue);
			}
		}
		if (!m)
			return {red: this.clamp(red), green: this.clamp(green), blue: this.clamp(blue)};
		m /= this.applet.gamma;
		this.applet.gamma_red = this.clamp(red * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
		this.applet.gamma_green = this.clamp(green * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
		this.applet.gamma_blue = this.clamp(blue * m, this.applet.minimum_gamma, this.applet.maximum_gamma);
	}
}
