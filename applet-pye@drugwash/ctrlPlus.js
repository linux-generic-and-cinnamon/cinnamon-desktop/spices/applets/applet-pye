/* THIS FILE IS PART OF THE 'APPLET PYE' APPLET FOR CINNAMON */
/* based on the original popupMenu.js version in Cinnamon 4.2 */
/* if you want something done do it yourself ! (c) Drugwash, 2021 */
const Cairo = imports.cairo;
const Cinnamon = imports.gi.Cinnamon;
const Clutter = imports.gi.Clutter;
const Cogl = imports.gi.Cogl;
const GdkPixbuf = imports.gi.GdkPixbuf;
const Lang = imports.lang;
const Signals = imports.signals;
const St = imports.gi.St;
const Tooltips = imports.ui.tooltips;
const Params = imports.misc.params;
const PopupMenu = imports.ui.popupMenu;
/*=========================================================================================*/
var PopupSliderPlusMenuItem = class PopupSliderPlusMenuItem extends PopupMenu.PopupBaseMenuItem {
	_init(icon, value, params) {
		super._init.call(this, { activate: false });
		var par = Params.parse (params, {
				iSize: 12,			// icon size
				iType: St.IconType.FULLCOLOR,
				text: "value",		// tooltip text (left)
				fStyle: "normal",	// label font style (weight)
				fSize: "8pt",		// label font size
				units: "%",			// label+tooltip value units (right)
				img: '',				// slider background image
				kDelta: 0.1,		// key delta
				sStep: 0.01		// scroll step
		});
		this._signals.connect(this.actor, 'key-press-event', Lang.bind(this, this._onKeyPressEvent));

		if (isNaN(value))
			// Avoid spreading NaNs around
			throw TypeError('The slider value must be a number');
		this._value = Math.max(Math.min(value, 1), 0); let fval = Math.round(this._value * 100);
		this.units = par.units; this.text = par.text; this.fSize = par.fSize; this.fStyle = par.fStyle; this._img = par.img;
		this.kDelta = par.kDelta; this.sStep = par.sStep;

		if (typeof(icon) == 'string')
			this._icon = new St.Icon({icon_name: icon, icon_type: par.iType, icon_size: par.iSize, reactive: false});
		else if (typeof(icon) == 'object') {
			this._icon = new St.Icon({icon_name: "", icon_type: par.iType, icon_size: par.iSize, reactive: false});
			this._icon.set_gicon(icon);
		} else this._icon = new St.Icon({icon_type: par.iType, icon_size: par.iSize, reactive: false});

		this._slider = new St.DrawingArea({ style_class: 'popup-slider-menu-item', reactive: true });
		if (this._img) {
			try {
				this.colorlabel = new Clutter.Texture({width: 0, height: 0, load_async: true,
				keep_aspect_ratio: false, filter_quality: 2, filename: this._img});
				global.log("Clutter.Texture() still works")
			} catch(e) {
				let img = new Clutter.Image();
				let pixbuf = GdkPixbuf.Pixbuf.new_from_file(this._img);
				img.set_data(pixbuf.get_pixels(),
					(pixbuf.get_has_alpha()) ? Cogl.PixelFormat.RGBA_8888 : Cogl.PixelFormat.RGB_888,
					pixbuf.get_width(),
					pixbuf.get_height(),
					pixbuf.get_rowstride());
				this.colorlabel = new Clutter.Actor();
				this.colorlabel.set_content(img);
				global.log("Clutter.Image() is the 'improvement'")
			}
//			this._slider.add_actor(this.colorlabel, {span: -1, expand: true, align: St.Align.START});
			this._slider.add_actor(this.colorlabel);
		}
		this.label = new St.Label({ text: fval + this.units, reactive: false, important: true });
		this.tooltip = new Tooltips.Tooltip(this._slider, this.text + ": " + fval + this.units);

		this._icon.set_style('margin: 1px; padding: 1px;');	// border: 1px solid red;
		this.labelStyle = "font-size: " + this.fSize + "; font-weight: " + this.fStyle +
			"; text-align: right; border: none; padding: 1px 3px 1px 5px; margin: 0px;";
		this.label.set_style(this.labelStyle);
		this._slider.set_style('border: none; margin: 1px; padding: 0px;');

		this.addActor(this._icon, {span: 0, expand: false, align: St.Align.START});
		this.addActor(this._slider, { span: 0, expand: true });
		this.addActor(this.label, {span: 0, expand: false, align: St.Align.END});
		this._signals.connect(this._slider, 'repaint', Lang.bind(this, this._sliderRepaint));
		this._signals.connect(this.actor, 'button-press-event', Lang.bind(this, this._startDragging));
		this._signals.connect(this.actor, 'scroll-event', Lang.bind(this, this._onScrollEvent));

		this._releaseId = this._motionId = 0;
		this._dragging = false;
		this._mark_position = 0;	// 0 means no mark
		this._minimarks = 0;		// 0 means no mini marks
		this.actor.set_style('margin: 0; padding: 0; spacing: 0; span: 0; expand: false;'); // border: 1px solid blue;
		this._spacing = 0;
	}

	setValue(value) {
		if (isNaN(value))
			throw TypeError('The slider value must be a number');
		this._value = Math.max(Math.min(value, 1), 0);
		this._slider.queue_repaint();
	}

	setText(text) {
		this.text = text;
	}

	setUnits(units) {
		this.units = units;
	}

	_sliderRepaint(area) {
		let cr = area.get_context();
		let themeNode = area.get_theme_node();
		let [width, height] = area.get_surface_size();
		if (this._img) { this.colorlabel.width = width; this.colorlabel.height = height; }
		let handleRadius = themeNode.get_length('-slider-handle-radius');

		let sliderWidth = width - 2 * handleRadius;
		let sliderHeight = themeNode.get_length('-slider-height');

		let sliderBorderWidth = themeNode.get_length('-slider-border-width');
		let sliderBorderRadius = Math.min(width, sliderHeight) / 2;

		let sliderBorderColor = themeNode.get_color('-slider-border-color');
		let sliderColor = themeNode.get_color('-slider-background-color');

		let sliderActiveBorderColor = themeNode.get_color('-slider-active-border-color');
		let sliderActiveColor = themeNode.get_color('-slider-active-background-color');

//___ Draw a mark to indicate a certain value
		if (this._mark_position > 0) {
			Clutter.cairo_set_source_color(cr, (this.active) ? themeNode.get_foreground_color() : sliderActiveColor);
			let markWidth = 2;
			let markHeight = (height - sliderHeight) / 2;
			let xMark = handleRadius / 2 + sliderWidth * this._mark_position + markWidth;
			let yMark = height - markHeight;
			cr.rectangle(xMark, yMark, markWidth, markHeight);
			cr.fill();
		}
//___ Draw mini marks if enabled
		if (this._minimarks > 0) {
			Clutter.cairo_set_source_color(cr, (this.active) ? themeNode.get_foreground_color() : sliderActiveColor);
			let markWidth = 1;
			cr.setLineWidth(markWidth);
			let markHeight = (height - sliderHeight) / 4;
			let yMark = height - markHeight;
			for (let i = 0; i < sliderWidth * this._minimarks; i++) {
				let xMark = handleRadius / 2 + sliderWidth * (i * this._minimarks) + 3;
				cr.moveTo(xMark, yMark); cr.lineTo(xMark, yMark + markHeight);
				cr.stroke();
			}
		}

		const TAU = Math.PI * 2;

		let handleX = handleRadius + (width - 2 * handleRadius) * this._value;
// PROGRESSBAR
		if (!this._img) {
			cr.arc(sliderBorderRadius + sliderBorderWidth, height / 2, sliderBorderRadius, TAU * 1/4, TAU * 3/4);
			cr.lineTo(handleX, (height - sliderHeight) / 2);
			cr.lineTo(handleX, (height + sliderHeight) / 2);
			cr.lineTo(sliderBorderRadius + sliderBorderWidth, (height + sliderHeight) / 2);
			Clutter.cairo_set_source_color(cr, sliderActiveColor);
			cr.fillPreserve();
			Clutter.cairo_set_source_color(cr, sliderActiveBorderColor);
			cr.setLineWidth(sliderBorderWidth);
			cr.stroke();

			cr.arc(width - sliderBorderRadius - sliderBorderWidth, height / 2, sliderBorderRadius, TAU * 3/4, TAU * 1/4);
			cr.lineTo(handleX, (height + sliderHeight) / 2);
			cr.lineTo(handleX, (height - sliderHeight) / 2);
			cr.lineTo(width - sliderBorderRadius - sliderBorderWidth, (height - sliderHeight) / 2);
			Clutter.cairo_set_source_color(cr, sliderColor);
			cr.fillPreserve();
			Clutter.cairo_set_source_color(cr, sliderBorderColor);
			cr.setLineWidth(sliderBorderWidth);
			cr.stroke();
		}
// PROGRESS THUMB
		let handleY = height / 2;
		let color = themeNode.get_foreground_color();
		Clutter.cairo_set_source_color(cr, color);
// we modified this thumb drawing for image slider
		if (!this._img)
			cr.arc(handleX, handleY, handleRadius, 0, 2 * Math.PI);
		else {
			let x1 = Math.round(handleX - handleRadius + 1);
			let x2 = Math.round(handleX + handleRadius - 1);
			let y1 = Math.round(height * 0.425);
			let y2 = Math.round(height * 0.85);
			handleX = Math.round(handleX);
			cr.moveTo(x1, 0); cr.lineTo(x1, y1); cr.lineTo(handleX, y2); cr.lineTo(x2, y1); cr.lineTo(x2, 0);
			cr.closePath();
		}
		cr.fillPreserve();
		Clutter.cairo_set_source_color(cr, sliderActiveColor);
		cr.setLineWidth(1);
		cr.stroke();

		cr.$dispose();
	}

	_startDragging(actor, event) {
		if (this._dragging) // don't allow two drags at the same time
			return;

		this.emit('drag-begin');
		this._dragging = true;

		// FIXME: we should only grab the specific device that originated
		// the event, but for some weird reason events are still delivered
		// outside the slider if using clutter_grab_pointer_for_device
		try { event.get_device().grab(this._slider); }
		catch(e) { Clutter.grab_pointer(this._slider); }
		this._signals.connect(this._slider, 'button-release-event', Lang.bind(this, this._endDragging));
		this._signals.connect(this._slider, 'motion-event', Lang.bind(this, this._motionEvent));
		let absX, absY;
		[absX, absY] = event.get_coords();
		this._moveHandle(absX, absY);
	}

	_endDragging(actor, event) {
		if (this._dragging) {
			this._signals.disconnect('button-release-event', this._slider);
			this._signals.disconnect('motion-event', this._slider);

			try { event.get_device().ungrab(); }
			catch(e) { Clutter.ungrab_pointer(); }
			this._dragging = false;
// we leave it enabled since people tend to forget 'drag-end' exists
			this.emit('value-changed', this._value);
			this.emit('drag-end');
	  }
		return true;
	}

	_onScrollEvent (actor, event) {
		let direction = event.get_scroll_direction();
		if (direction == Clutter.ScrollDirection.SMOOTH)
            return;
		let mods = Cinnamon.get_event_state(event);
		let m = (mods & Clutter.ModifierType.CONTROL_MASK) ? 5 :
			(mods & Clutter.ModifierType.SHIFT_MASK) ? 10 : 1;
		if (direction == Clutter.ScrollDirection.DOWN) {
			this._value = Math.max(0, this._value - m*this.sStep);
		}
		else if (direction == Clutter.ScrollDirection.UP) {
			this._value = Math.min(1, this._value + m*this.sStep);
		}

		this._slider.queue_repaint();
		this.emit('value-changed', this._value);
	}

	_motionEvent(actor, event) {
		let absX, absY;
		[absX, absY] = event.get_coords();
		this._moveHandle(absX, absY);
		return true;
	}

	_moveHandle(absX, absY) {
		let relX, relY, sliderX, sliderY;
		[sliderX, sliderY] = this._slider.get_transformed_position();
		relX = absX - sliderX;
		relY = absY - sliderY;

		let width = this._slider.width;
		let handleRadius = this._slider.get_theme_node().get_length('-slider-handle-radius');

		let newvalue;
		if (relX < handleRadius)
			newvalue = 0;
		else if (relX > width - handleRadius)
			newvalue = 1;
		else
			newvalue = (relX - handleRadius) / (width - 2 * handleRadius);

		this._value = newvalue;
		this._slider.queue_repaint();
		this.emit('value-changed', this._value);
	}

	get value() {
		return this._value;
	}

	set_mark (value) {
		this._mark_position = value;
	}

	set_minimarks (value, min, max) {
		this._minimarks = value / (max - min);
	}

	_onKeyPressEvent (actor, event) {
		const key = event.get_key_symbol();
		if (key == Clutter.KEY_Right || key == Clutter.KEY_Left) {
			let delta = key == Clutter.KEY_Right ? this.kDelta : -this.kDelta;
			let mods = Cinnamon.get_event_state(event);
			let m = (mods & Clutter.ModifierType.CONTROL_MASK) ? 5 :
				(mods & Clutter.ModifierType.SHIFT_MASK) ? 10 : 1;
			delta *= m;
			this._value = Math.max(0, Math.min(this._value + delta, 1));
			this._slider.queue_repaint();
			this.emit('value-changed', this._value);
			this.emit('drag-end');
			return true;
		}
		return false;
	}
}
/*=========================================================================================*/
var PopupSeparatorPlusMenuItem = class PopupSeparatorPlusMenuItem extends PopupMenu.PopupBaseMenuItem {
	_init (params) {
		super._init.call(this, { reactive: false });
		var par = Params.parse (params, {
				margin: 0,
				fPath: '',
				width: 20,
				height: 2,
				aspect: false
		});
		this.margin = par.margin; this.width = par.width; this.height = par.height;
		this.actor.set_style_class_name("");
		this.actor.set_style('border: none; margin: " + this.margin + "px; padding: 0px;');
		if (!par.fPath || par.fPath == 'undefined') {
			this._drawingArea = new St.DrawingArea({ style_class: 'popup-separator-menu-item' });
			this.addActor(this._drawingArea, { span: -1, expand: true, align: St.Align.MIDDLE });
			this._signals.connect(this._drawingArea, 'repaint', Lang.bind(this, this._onRepaint));
		} else {
			let cl;
			try {
				cl = new Clutter.Texture({width: this.width, height: this.height, keep_aspect_ratio: par.aspect,
				load_async: true, filter_quality: 2, filename: par.fPath});
			} catch(e) {
				let img = new Clutter.Image();
				let pixbuf = GdkPixbuf.Pixbuf.new_from_file(par.fPath);
				img.set_data(pixbuf.get_pixels(),
					(pixbuf.get_has_alpha()) ? Cogl.PixelFormat.RGBA_8888 : Cogl.PixelFormat.RGB_888,
					pixbuf.get_width(),
					pixbuf.get_height(),
					pixbuf.get_rowstride());
				cl = new Clutter.Actor();
				cl.set_content(img);
				cl.height = this.height; cl.width = this.width;
			}
			this.addActor(cl, { span: -1, expand: true, align: St.Align.MIDDLE });
		}
	}

	_onRepaint(area) {
		let cr = area.get_context();
		let themeNode = area.get_theme_node();
		let [width, height] = area.get_surface_size();
		let margin = themeNode.get_length('-margin-horizontal');
		let gradientHeight = themeNode.get_length('-gradient-height');
		let startColor = themeNode.get_color('-gradient-start');
		let endColor = themeNode.get_color('-gradient-end');

		let gradientWidth = (width - margin * 2);
		let gradientOffset = (height - gradientHeight) / 2;
		let pattern = new Cairo.LinearGradient(margin, gradientOffset, width - margin, gradientOffset + gradientHeight);
		pattern.addColorStopRGBA(0, startColor.red / 255, startColor.green / 255, startColor.blue / 255, startColor.alpha / 255);
		pattern.addColorStopRGBA(0.5, endColor.red / 255, endColor.green / 255, endColor.blue / 255, endColor.alpha / 255);
		pattern.addColorStopRGBA(1, startColor.red / 255, startColor.green / 255, startColor.blue / 255, startColor.alpha / 255);
		cr.setSource(pattern);
		cr.rectangle(margin, gradientOffset, gradientWidth, gradientHeight);
		cr.fill();

		cr.$dispose();
	}
}
