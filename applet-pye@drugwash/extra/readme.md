﻿**Applet PYE** - xlet for the Linux Cinnamon desktop
***
### LICENSE
***
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.<br/>
***
### ATTRIBUTION
***
- This code was initially based on [Brightness and gamma applet](https://cinnamon-spices.linuxmint.com/applets/view/286) by **cardsurf** and
it still contains large parts of it. Thank you **@cardsurf**
- The code makes use of the public APIs kindly provided by:
[kwelo.com](https://www.kwelo.com/network) and [sunrise-sunset.org](https://sunrise-sunset.org/)
- Image (cropped and resized) used for color temperature is provided here
courtesy of **Bhutajata** at [Wikipedia](https://en.wikipedia.org/wiki/Color_temperature)
licensed as [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
- Symbolic icons are part of the [120 UI Pack Icon](https://dribbble.com/shots/6592214-120-UI-Pack-icon-free-download) by Rudez
- Color icons are part of the [Pastel SVG](https://codefisher.org/pastel-svg/) large pack by Michael Buckley
	licensed as [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0)
- More inspiration came from various sources such as:

	- [Show Remote IP](https://cinnamon-spices.linuxmint.com/desklets/view/48) desklet by **Nejdet Çağdaş Yücesoy**
	- [Num Lock/Caps Lock indicator with notifications](https://cinnamon-spices.linuxmint.com/applets/view/83) applet by **entelechy**
	- [Shutdown Menu With Icons](https://cinnamon-spices.linuxmint.com/applets/view/114) applet by **Nicolas Llobera**
	- built-in **Sound** applet by **cinnamon\.org**
	- [Spices Update](https://cinnamon-spices.linuxmint.com/applets/view/309) applet by **claudiux**
	- the world wide web...<br/>

***
### ABOUT
***
**T**he name is a wordplay on 'apple pie'. **[PYE](https://www.allacronyms.com/PYE/Protect_Your_Eyes)** actually stands for **P**rotect **Y**our **E**yes.<br/>
People with sensitive eyes may need different levels of screen brightness and/or gamma,
and often the levels of color temperature should differ between day and night.
This applet aims to provide easy access to the aforementioned settings,
and even attempts to provide a fully automated environment by retrieving and using
the sunrise, sunset and twilight time values from online services in order to provide
a gradual switch from day levels to night levels and vice-versa.

**T**here are four status modes available: *Day*, *Night*, *Custom*, and *Auto*:<br/>

- **Day** - the most common mode. All default levels are probably at 100% or close.
- **Night** - lower brightness and warmer color temperature. Easy on the eyes.
- **Custom** - all settings are freely adjusted by the user as desired, at any time.
- **Auto** - levels are automatically set and gradually switched according to current time,
considering the sunrise, sunset, and twilight time values retrieved online or manually set.

**T**here also is a superset of all the above, triggered by the *Cloudy* mode, if enabled.
The *Cloudy* mode applies a user-defined decrease to brightness and/or gamma levels.
This is only applied on-the-fly for current session and does not carry to future sessions.

**I**deally, in online update mode the code should only access the services once a day.
Subsequent access would only be required when the user is traveling with the computer
to distant locations where timezone is different and so are sunrise and sunset values.
**T**here is no personal or private information sent over the web during update.
Actually there is no data sent whatsoever other than the requests to GET the information.

**I**f online access is missing or forbidden, the user can manually set the approximate time
values for sunrise and sunset, and the code will react based on those values.
Such manual operating mode will however be less accurate than the online mode.

**V**alues for *Day* and *Night* modes are fixed thresholds, established in the **Settings** panel.
Even if the user modifies them at run time, they will revert to default upon applet startup -
which can be: applet manual reload, Cinnamon reload, user log off/on, reboot, computer start.

**V**alues for *Custom* mode are saved between sessions, so if user leaves current session
regardless of the current mode, at next startup those values will be restored when entering *Custom* mode.
***
**T**here are two ways of operating this applet: through the sliders interface or by mouse+keys.
However not all operations are available through sliders. See below.

-  The sliders interface is a menu that appears upon left-clicking applet's panel icon.
	There, the user can drag the knobs to set the desired values. Changes are in real-time.
	Mouse wheel can be used too while hovering the desired slider.
	User can select which sliders are shown by operating changes in the **Settings > GUI** dialog.
-  The panel icon allows the user to quickly modify levels through combinations of
	keypresses, mouse button clicks and/or mouse wheel scrolling.
	Following info can also be found in the Settings dialog; it's here for completeness.

	- **Left-click** toggles sliders menu
	- **Ctrl+Left-click** toggles Cloudy mode (if enabled)
	- **Middle-click** switches modes forwards
	- **Ctrl+Middle-click** switches modes backwards
	- **Right-click** shows configuration menu

	**I**f wheel scrolling is enabled:

	- **Wheel** alone adjusts brightness
	- **Shift+Wheel** adjusts gamma
	- **Ctrl+Wheel** adjusts color temperature

**T**hat's about it. There may be bugs, I apologyze in advance. Please [notify me](mailto:drugwash@mail.com)
and I'll try to fix them as soon as possible, if possible. Any new ideas are also welcome.

***© Drugwash 2021.04.12***
