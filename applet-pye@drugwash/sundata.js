/* THIS FILE IS PART OF THE 'APPLET PYE' APPLET FOR CINNAMON */
//imports.gi.versions.Soup = "2.4";
const Gio = imports.gi.Gio;				// because of the libsoup changes
const ByteArray = imports.byteArray;	// same as above
const St = imports.gi.St;
const Soup = imports.gi.Soup;
const Util = imports.misc.util;
const Lang = imports.lang;

var API0 = "https://ipwho.is/?fields=ip,timezone.id,latitude,longitude,city,country_code,country,connection.isp";
var API1 = "https://api.kwelo.com/v1/network/ip-address/my";		// no params
var API2 = "https://api.kwelo.com/v1/network/ip-address/location/";	// param: IP [xxx.xxx.xxx.xxx]
var API3 = "https://api.sunrise-sunset.org/json?formatted=0";		// params: lat [float], lng [float], date [today/yyyy-MM-dd/etc]
var defF = "ro-RO";	// "ro-RO" shows time in 24h format; for 12h use "en-US"
var defTZ = "Europe/Bucharest";
var TimeNow = ""; var msg = ""; var ctp = 10;	// color temp. precision [K]

try {
	let m1 = Soup.get_major_version();
	let m2 = Soup.get_minor_version();
	let m3 = Soup.get_micro_version();
	if (!Soup.check_version("2", "42", "0"))
		global.logError("Soup version mismatch: " + m1 + "." + m2 + "."  + m3 + " < at least 2.42.0 required.");
	else global.log("Soup version: " + m1 + "." + m2 + "."  + m3);
} catch (e) { global.logError("Soup version check: " + e); }

const APIreq = new Soup.Session();
APIreq.user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:115.0) Gecko/20100101 Firefox/115.0";
APIreq.timeout = 5; APIreq.idle_timeout = 5;
if (Soup.MAJOR_VERSION === 2) {
	APIreq.add_feature(new Soup.ProxyResolverDefault());
// Not secure but at least it freakin' works on api.sunrise-sunset.org
// since their certificate expired (cca. 2022.04.16)
// Or at least used to work before libsoup changes
	APIreq["ssl-strict"]=false;
}
else {
	try { APIreq.set_proxy_resolver(Gio.ProxyResolver.get_default()); }
	catch(e) { tellEm(e); }
}

function tellEm(e="") { global.logWarning("Thanks libsoup masters!"+(e?" => "+e:"")); };
function sun(main) {
	this._init(main);
};

sun.prototype = {
	_init: function(main) {
		this.main = main;
		this.timeout = 0; this.secTimeout = 0; this.cds = 0; this.lds = 0;
		this.bri_up = 0; this.bri_dn = 0; this.dbg = this.main.dbgsw;
		this.wallet = []; this.tObj = [];
		let fields = ['ip', 'cdt', 'lat', 'lon', 'tzn', 'snr', 'sns', 'ctb', 'cte', 'ntb', 'nte', 'dln', 'isp', 'cit', 'ccd', 'cty', 'upd'];
		for (let i=0; i<fields.length; i++) { this.wallet[fields[i]] = ""; }
		this.wallet['tzn'] = defTZ;
		this.msg = ["Nautical twilight end", "Nautical twilight begin", "Civil twilight begin",
			"Sunrise", "Sunset", "Civil twilight end", "Nautical twilight end"];
		TimeNow = this._localTime();
		if (this.main.screen_mode == 3) this.timer(true);
		else { global.log("We're in mode " + this.main.screen_mode); this.getDayslice(); }
	},
	acceptCert: function() { return true; },
	timer: function(on) {
		if (on) {
			this.updateData(this.main.auto_mode); this.updateTime();
			this.timeout = Util.setInterval(() => this.updateTime(), (this.dbg) ? 999 : 59999);
			this.secTimer(true);
		}
		else if (!on && this.timeout) { Util.clearInterval(this.timeout); this.timeout = null; this.secTimer(false); }
//		if (!this.dbg) global.log("Timer ID=" + this.timeout + " enabled=" + on);
	},
	secTimer: function(on) {
		if (on) this.secTimeout = Util.setInterval(() => this.main.tickTock(), 499);
		else if (!on && this.secTimeout) {
			Util.clearInterval(this.secTimeout);
			this.secTimeout = null;
			this.main.tock = "";
			this.main.update_tooltip_values();
		}
	},
	_comm: function(msg) {
		let sc, rp, res;
		if (Soup.MAJOR_VERSION === 2) {
			APIreq.send_message(msg);
			sc = msg.status_code;
			rp = msg.reason_phrase;
			res = msg.response_body.data;
		} else {
			msg.connect("accept-certificate", () => this.acceptCert());
			let bytes = APIreq.send_and_read(msg, null, null);
			sc = msg.get_status();
			rp = msg.get_reason_phrase();
			res = ByteArray.toString(bytes.get_data());
		}
		return [sc, rp, res];
 	},
	getLocation: function() {
		let message = Soup.Message.new("GET", API0);
		let [sc, rp, res] = this._comm(message);
		if (sc === 200) {
			var locdata = JSON.parse(res);
			try { Util.unref(res); } catch(e) {tellEm(e);}
			let ip = locdata.ip;
			if (ip == this.wallet['ip']) return ip;
			this.wallet['ip'] = ip;
			let lat = locdata.latitude.toFixed(4);
			let lon = locdata.longitude.toFixed(4);
			let tzn = locdata.timezone.id;
			if (this.dbg) global.log("Lat: " + lat + " Long: " + lon + " Tzn: " + tzn);
//___compare data in the wallet object with current one and avoid superfluous retrieval if identical
			if (lat == this.wallet['lat'] && lon == this.wallet['lon']) return ip;
			let cit = locdata.city;
			let ccd = locdata.country_code;
			let cty = locdata.country;
			let isp = locdata.connection.isp;
			this.wallet['lat'] = lat; this.wallet['lon'] = lon; this.wallet['tzn'] = tzn;
			this.wallet['cit'] = cit; this.wallet['ccd'] = ccd; this.wallet['cty'] = cty;
			this.wallet['isp'] = isp;
			if (this.dbg) global.log("IP: " + this.wallet['ip'] + " Lat: " + this.wallet['lat'] +
				" Lon: " + this.wallet['lon'] + " Tzn: " + this.wallet['tzn'] +
				" Country: " + this.wallet['cty'] + " Code: " + this.wallet['ccd'] +
				" City: " + this.wallet['cit'] + " ISP: " + this.wallet['isp']);
			return ip;
		} else {
			global.logError("Error in getLocation(): status code " + sc + ": " + rp);
			return false;
		}
	},
	getSundata: function(ip) {
		let cdt = (new Date()).toLocaleDateString(defF, {timeZone: this.wallet['tzn']});
		if (this.dbg) global.log("begin API call 3");
		if (cdt == this.wallet['cdt'] && ip == this.wallet['ip']) return true;
		let lat = this.wallet['lat']; let lon = this.wallet['lon'];
//		if (!lat && !lon) { lat=44.9417468; lon=26.0236504; } // this should better be turned into manual setting
		let apiCall3 = API3 + "&lat=" + lat + "&lng=" + lon + "&date=" + cdt;
		if (this.dbg) global.log("API call 3: " + apiCall3);
		let message = Soup.Message.new("GET", apiCall3);
//		try { message.set_flags(512); } // SOUP_MESSAGE_DO_NOT_USE_AUTH_CACHE (v2.58+)
//		catch (e) { global.logError("set_flags 'do-not-use-auth-cache' didn't work."); }
		try { message.set_flags(32); } // SOUP_MESSAGE_CERTIFICATE_TRUSTED
		catch (e) { global.logError("set_flags 'certificate-trusted' didn't work."); }
		let [sc, rp, res] = this._comm(message);
		if (sc === 200) {
			var sundata = JSON.parse(res);
			try { Util.unref(res); } catch(e) {tellEm(e);}
			this.wallet['snr'] = this._localTime(sundata.results.sunrise, defF, this.wallet['tzn']);
			this.wallet['sns'] = this._localTime(sundata.results.sunset, defF, this.wallet['tzn']);
			this.wallet['ctb'] = this._localTime(sundata.results.civil_twilight_begin, defF, this.wallet['tzn']);
			this.wallet['cte'] = this._localTime(sundata.results.civil_twilight_end, defF, this.wallet['tzn']);
			this.wallet['ntb'] = this._localTime(sundata.results.nautical_twilight_begin, defF, this.wallet['tzn']);
			this.wallet['nte'] = this._localTime(sundata.results.nautical_twilight_end, defF, this.wallet['tzn']);
			this.wallet['dln'] = new Date(sundata.results.day_length * 1000).toISOString().substr(11, 8);
			this.wallet['upd'] = this._localDate() + ", " + this._localTime();
			this.wallet['cdt'] = cdt;
			if (this.dbg) global.log("Sunrise: " + this.wallet['snr'] + " Sunset: " + this.wallet['sns'] + " Day length: " + this.wallet['dln']);
			this.getDayslice(); this.main.refreshLocinfo(this.cds, this.wallet, this.msg);
			return true;
		} else {
			global.logError("Error in getSundata(): status code " + sc + ": " + rp);
			return false;
		}
	},
//_convert [HH:mm:ss] time into Unix timestamp [ms]
	getMS: function(t, off = 0) {
		let a = t.split(":"); let x = +a[1] + off; let h, m;
		if (x < 0) { h = +a[0] - 1; m = 60 + x; }
		else if (x > 59) { h = +a[0] + 1; m = x - 60; }
		else { h = +a[0]; m = x; }
		return new Date().setHours(h, m, 0, 0);
	},
//_calculate accurate thresholds based on sundata
	getSunmarkers: function(now, sd, sn) {
		let n1, n2, n3, d1, d2, d3;
//global.log("sd=" + sd + ", sn=" + sn);
		if (sd === undefined) sd = 0; if (sn === undefined) sn = 0;
		now.setSeconds(0, 0); let tn = now.getTime();
		d1 = this.getMS(this.wallet['snr'], sd);
		d2 = this.getMS(this.wallet['sns'], -sn);
		d3 = this.getMS(this.wallet['cte']); n3 = this.getMS(this.wallet['ctb']);
		n2 = this.getMS(this.wallet['ntb']); n1 = this.getMS(this.wallet['nte']);
		return [ tn, d1, d2, d3, n3, n2, n1 ];	// easier to pass to functions
	},
//_perform all checks here
	getDayslice: function() {
		let ct = new Date();
		if (this.main.auto_mode == 2)
			this.tObj = this.getSunmarkers(ct, this.main.shift_day, this.main.shift_night);
		else if (this.main.auto_mode < 2)
			this.tObj = this.getMarkers(30, 30, this.main.shift_day, this.main.shift_night);
		else {
			global.logError("Error: auto_mode " + this.main.auto_mode +
				". We shouldn't be in getDayslice() for auto_modes other than 1 or 2 !");
			return;
		}
 		if (this.dbg) global.log("auto mode " + this.main.auto_mode + " " + ct.toString() + " => " +
			this.tObj[0] + "\n" + this.tObj[1] + ", " + this.tObj[2] + "\n" + this.tObj[3] + ", " +
			this.tObj[4] + "\n" + this.tObj[5] + ", " + this.tObj[6]);
		let msg = this.getCDS(this.tObj);
		if (this.cds != this.lds) { this.lds = this.cds; if (this.dbg) global.log(msg + " at " + TimeNow); }
		if (this.main.auto_mode < 1) return;
		if (this.cds == 2 || this.cds == 3) {
		this.bri_up = this._calcVal(this.main.brightness_day, this.main.brightness_night, this.tObj,true);
		this.gam_up = this._calcVal(this.main.gamma_day, this.main.gamma_night, this.tObj, true);
		this.clr_up = this._calcVal(this.main.colortemp_day, this.main.colortemp_night, this.tObj, true);
		this.clr_up = Math.ceil(this.clr_up / ctp) * ctp;
		this.bri_dn = "\uFFEA"; this.gam_dn = "\uFFEA"; this.clr_dn = "\uFFEA";
		} else if (this.cds == 5 || this.cds == 6) {
		this.bri_dn = this._calcVal(this.main.brightness_day, this.main.brightness_night, this.tObj);
		this.gam_dn = this._calcVal(this.main.gamma_day, this.main.gamma_night, this.tObj);
		this.clr_dn = this._calcVal(this.main.colortemp_day, this.main.colortemp_night, this.tObj);
		this.clr_dn = Math.floor(this.clr_dn / ctp) * ctp;
		this.bri_up = "\uFFEC"; this.gam_up = "\uFFEC"; this.clr_up = "\uFFEC";
		}
		let b = 0; let c = 0; let g = 0; let d = false;
		if (this.main.cloudy_on && this.main.cloudy) {
			b = this.main.cloudy_dark; c = this.main.cloudy_color; g = this.main.cloudy_gamma; d = true;
		}
/* THIS SECTION APPLIES CALCULATED VALUES FOR BRIGHTNESS, GAMMA AND COLOR TEMP */
		if (this.cds == 1 && this.tObj[0] == this.tObj[6]) {
			this.main.brightness = this.main.brightness_night - b;
			this.main.gamma = this.main.gamma_night - g;
			this.main.colortemp = this.main.colortemp_night - c;
		} else if (this.cds == 2 || this.cds == 3) {
			this.main.brightness = this.bri_up - b;
			this.main.gamma = this.gam_up - g;
			this.main.colortemp = this.clr_up - c;
		} else if (this.cds == 4 && this.tObj[0] == this.tObj[1]) {
			this.main.brightness = this.main.brightness_day - b;
			this.main.gamma = this.main.gamma_day - g;
			this.main.colortemp = this.main.colortemp_day - c;
		} else if (this.cds == 5 || this.cds == 6) {
			this.main.brightness = this.bri_dn - b;
			this.main.gamma = this.gam_dn - g;
			this.main.colortemp = this.clr_dn - c;
		}
		if (this.main.screen_mode == 3) {
		this.main.brightness_auto = this.main.brightness;
		this.main.gamma_auto = this.main.gamma;
		this.main.colortemp_auto = this.main.colortemp;
		}
//		try { this.main.updateAll(); } catch(e) { global.logWarning("Sliders menu not yet initialized. OK at startup."); }
		if (this.main.menu_sliders) this.main.updateAll();
/* DISABLE (COMMENT OUT) THE SECTION ABOVE IF VALUES GO WRONG (OUT OF RANGE) */
		if (this.dbg) global.log("Time is " + TimeNow + " in dayslice " + this.cds +
				(d ? ", cloudy [br=" + b + "%, clr=" + c + "K, gam=" + g + "%]" : "") +
				"\nbrightness is " + this.bri_up + " " + this.bri_dn + (c ? " ( - " + c + "%)" : "") +
				"\ngamma is " + this.gam_up + " " + this.gam_dn +
				"\ncolortemp is " + this.clr_up + " " + this.clr_dn);
	},
	_calcVal: function(d, n, o, dir) {		// val := vmin+((tnow - t1)*(vmax - vmin))/(t2 - t1)
		if (dir) return Math.round(n + Math.pow(d - n, (o[0] - o[5])/(o[1] - o[5])));
		else return Math.round(n + Math.pow(d - n, (o[6] - o[0])/(o[6] - o[2])));
	},
//_this sets time markers for twilights and sunrise/sunset in offline mode.
//_allows post-sunrise & pre-sunset delays. t1=nautical, t2=civil, t3=sunrise delay, t4=sunset anticipation
	getMarkers: function(t1, t2, t3, t4) {
		let x = t1 + t2; let y = Math.floor(x / 61); let z = x - 60 * y; let tn = new Date().setSeconds(0, 0);
		let d1 = new Date().setHours(this.main.auto_day, t3, 0, 0);					// sunrise
		let d2 = new Date().setHours(this.main.auto_night - 1, 60 - t4, 0, 0);		// sunset
		let d3 = new Date().setHours(this.main.auto_night, t2, 0, 0);				// civil end
		let n3 = new Date().setHours(this.main.auto_day - 1, 60 - t2, 0, 0);		// civil begin
		let n2 = new Date().setHours(this.main.auto_day - y - 1, 60 - z, 0, 0);		// nautical begin
		let n1 = new Date().setHours(this.main.auto_night + y, z, 0, 0);			// nautical end
		if (this.dbg) global.log("getMarkers() => day=" + this.main.auto_day + " night=" + this.main.auto_night + " n2=" +
			this._localTime(n2) + " n3=" + this._localTime(n3) + " d1=" + this._localTime(d1) + " d2=" +
			this._localTime(d2) + " d3=" + this._localTime(d3) + " n1=" + this._localTime(n1));
		return [ tn, d1, d2, d3, n3, n2, n1 ];
	},
	getCDS: function(o) {
		let m = "Floating in the Universe";
		if		(o[0] >= o[6] || o[0] < o[5]) { this.cds = 1; m = this.msg[0]; }	// Nautical twilight end
		else if (o[0] >= o[5] && o[0] < o[4]) { this.cds = 2; m = this.msg[1]; }	// Nautical twilight begin
		else if (o[0] >= o[4] && o[0] < o[1]) { this.cds = 3; m = this.msg[2]; }	// Civil twilight begin
		else if (o[0] >= o[1] && o[0] < o[2]) { this.cds = 4; m = this.msg[3]; }	// Sunrise
		else if (o[0] >= o[2] && o[0] < o[3]) { this.cds = 5; m = this.msg[4]; }	// Sunset
		else if (o[0] >= o[3] && o[0] < o[6]) { this.cds = 6; m = this.msg[5]; }	// Civil twilight end
//		else global.logError("Time comparison screwed up at " + TimeNow + ", o[0]=" + o[0]);
		return m;
	},
	updateData: function(mode) {
		if (!this.dbg && mode == 2) {
//			global.log("Retrieving sundata...");
			this.main.animicon(true);
			let ip = this.getLocation();
			if (ip) {
				if (this.getSundata(ip)) {
//						global.log("Sundata retrieval complete");
					this.main.animicon(false);
					return true;
				} else { global.logError("updateData(): Failed to retrieve sundata"); return false; }
			} else { global.logError("updateData(): Failed to retrieve location data"); return false; }
		} else {
			if (!this.dbg) global.log("updateData(): Bypassed sundata retrieval in auto_mode " + mode);
			this.main.animicon(false);
			return false;
		}
	},
	_localDate: function(t, f, z) {
		if (!t) t = new Date(); if (!f) f = defF; if (!z) z = defTZ;
		return (new Date(t)).toLocaleDateString(f, {timeZone: z});
	},
	_localTime: function(t, f, z) {
		if (!t) t = new Date(); if (!f) f = defF; if (!z) z = defTZ;
		return (new Date(t)).toLocaleTimeString(f, {timeZone: z});
	},
	updateTime: function() {
		if (this.dbg || (!this.wallet['ip'] && this.main.auto_mode == 2)) this.updateData(this.main.auto_mode);
		else {
			TimeNow = this._localTime();
//__check if date changed and update sundata accordingly
			let cdt = this._localDate();
			if (cdt != this.wallet['cdt']) {
				if (this.main.auto_mode == 2) this.getSundata(this.wallet['ip']);
			}
			this.getDayslice();
			if (this.dbg) this._updateMsg();
		}
//		global.log("Timer runs, time is " + TimeNow);	// hogs CPU badly if timer is at 1 sec, don't use!!!
	},
	_updateMsg: function(msg) {
		global.log((msg ? msg : "") + "Applet PYE updated at " + TimeNow + ", Day slice: " + this.cds); return;
	}
};
