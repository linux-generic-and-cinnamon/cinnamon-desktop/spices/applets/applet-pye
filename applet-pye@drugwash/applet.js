/* THIS FILE IS PART OF THE 'APPLET PYE' APPLET FOR CINNAMON */
/* based on the original version for Brightness & Gamma applet by cardsurf */
// Applet PYE main script
const Applet = imports.ui.applet;
const AppletManager = imports.ui.appletManager;
const Cinnamon = imports.gi.Cinnamon;
const CDesk = imports.gi.CinnamonDesktop;
const Clutter = imports.gi.Clutter;
const Cogl = imports.gi.Cogl;
const Ext = imports.ui.extension;							// for path and icon theme, reload applet
const Gdk = imports.gi.Gdk;
const GdkPixbuf = imports.gi.GdkPixbuf;
const Gettext = imports.gettext;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Main = imports.ui.main;								// for notifications
const Mainloop = imports.mainloop;
const MessageTray = imports.ui.messageTray;		// for notifications
const Meta = imports.gi.Meta;								// for Settings window manipulation
const ModalDialog = imports.ui.modalDialog;
const PopupMenu = imports.ui.popupMenu;
const Settings = imports.ui.settings;
const St = imports.gi.St;
const Tooltips = imports.ui.tooltips;
const Util = imports.misc.util;
const uuid = "applet-pye@drugwash";
const aSig = "Applet PYE: ";							// applet signature for debug/error logs
const ICON_SIZE = 32;								// for notifications
const OPTS = { NONE: 0, SEL: 1, ALL: 2 };	// slider display options
const OPTA = { NO: 0, OF: 1, OL: 2 };			// auto options
const SEP0 = "============================================";	// separator line for settings dialog
const SEP = SEP0 + "=\n\t\t\t\t\t\t\t\t\t" + aSig;		// separator line for debug logs
var dbgsw = 0;											// debug switch
var settingsWindow;
let AppletGui, ShellUtils, CtrlPlus, Sundata, Values;
if (typeof require !== 'undefined') {
	AppletGui = require('./appletGui');
	ShellUtils = require('./shellUtils');
	CtrlPlus = require('./ctrlPlus');
	Sundata = require('./sundata');
	Values = require('./values');
} else {
	const AppletDirectory = imports.ui.appletManager.applets[uuid];
	AppletGui = AppletDirectory.appletGui;
	ShellUtils = AppletDirectory.shellUtils;
	CtrlPlus = AppletDirectory.ctrlPlus;
	Sundata = AppletDirectory.sundata;
	Values = AppletDirectory.values;
}

const MinXrandrVersion = 1.4;
const MinRandrVersion = 1.2;

// Translation support
Gettext.bindtextdomain(uuid, GLib.get_home_dir() + "/.local/share/locale")

function _(str) {
	return Gettext.dgettext(uuid, str);
}

function Output(output_name, is_connected, is_enabled) {
	this._init(output_name, is_connected, is_enabled);
}

Output.prototype = {
	_init: function(output_name, is_connected, is_enabled) {
		this.output_name = output_name;
		this.is_connected = is_connected;
		this.is_enabled = is_enabled;
	}
};

function AppletPYE(metadata, orientation, panel_height, instance_id) {
	this._init(metadata, orientation, panel_height, instance_id);
}

AppletPYE.prototype = {
	__proto__: Applet.IconApplet.prototype,

	_init: function(metadata, orientation, panel_height, instance_id) {
		Applet.IconApplet.prototype._init.call(this, orientation, panel_height, instance_id);
		this._meta = metadata;
		this.panel_orientation = orientation;
		this.applet_directory = this._get_applet_directory();
		this.mdFile = this.applet_directory + "extra/readme.md";
		this.txtFile = this.applet_directory + "extra/readme.txt";
		this.colormap_path = this.applet_directory + "extra/colormap.png";
		this.colormapt_path = this.applet_directory + "extra/colormap-t.png";
		this.colorsep_path = this.applet_directory + "extra/colorsep.png";
		this.autopath = this.applet_directory + "icons/hicolor/scalable/apps/sunA.svg";
		this.is_running = true;
		if (dbgsw) global.log(SEP + " started instance " + instance_id);
		try {
			this.RRscreen = new CDesk.RRScreen({ gdk_screen: Gdk.Screen.get_default() });
			this.RRscreen.init(null);
		} catch(e) {
			global.logError(e);
			this.RRscreen = null;
		}
		this.screen_outputs = {};
		this.menu_item_screen_position = 0;
		this.menu_item_outputs_position = 1;
		this.menu_item_screen = null;
		this.menu_item_outputs = null;
		this.menu_sliders = null;
		this.file_schema = "file://";
		this.home_shortcut = "~";
		this.xrandr_name = "xrandr";
		this.randr_name = "RandR";
		this.xrandr_regex = new RegExp(this.xrandr_name, "i");
		this.randr_regex = new RegExp(this.randr_name, "i");
		this.version_regex = new RegExp("[0-9]+[\.][0-9]+","i");
		this.screen_regex = new RegExp("screen.*:","i");
		this.number_regex = new RegExp("[0-9]+","");
		this.output_line_separator = " ";
		this.output_name_index = 0;
		this.output_status_index = 1;
		this.output_connected = "connected";
		this.output_disconnected = "disconnected";
		this.gamma_separator = ":";
		this.output_indexes_separator = "^";
		this.last_values_string = "";
		this.on_orientation_changed(orientation); // Initialize panel orientation

		this.settings = new Settings.AppletSettings(this, metadata.uuid, instance_id);
		this.default_screen_name = "";
		this.default_screen_mode = 0;				// default screen mode is Day
		this.default_brightness_day = 95;			// required for sliders
		this.default_brightness_custom = 85;
		this.default_gamma_day = 100;				// required for sliders
		this.default_gamma = 100;
		this.default_colortemp_day = 6500;		// required for sliders
		this.default_colortemp = 6500;
		this.default_output_indexes = [];
		this.minimum_colortemp = 3000;				// real value should be 4500
		this.maximum_colortemp = 10000;			// real value should be 9500
		this.brightness = this.default_brightness_day;
		this.gamma = this.default_gamma;
		this.gamma_red = this.default_gamma;
		this.gamma_green = this.default_gamma;
		this.gamma_blue = this.default_gamma;
		this.colortemp = this.default_colortemp;		// real value should be 6500
		this.screen_name = this.default_screen_name;
		this.output_indexes = this.default_output_indexes;
		this.cloudy = false;

		this.testmode = true;		// this should be 'false' for normal usage, or '2' for display debug
		this.tts = "";
		let c = this._applet_tooltip._tooltip.get_theme_node().get_color('color');
		let clr = (c.red + c.green + c.blue) / 381 < 1.0 ? "#770077" : "#FFDDFF";
		this.tthdr = "<span foreground='" + clr + "' size='medium'><b>%s %s</b></span>\n";
		this.SymStyle = "<span foreground='#FFFFFF' size='large'>%s</span>";
		this.tick = false;
		this.tock = " ";
		this.hdrstyle = "font-weight: bold; color: #EECCEE; font-size: 12px; " +
						"text-shadow: 1px 1px #000000; text-align: center;";
		this.screen_mode = this.default_screen_mode;
		this.max_mode = 3;
		this.smn = [_("Day"), _("Night"), _("Custom"), _("Auto")];
		this.smi = ["stock_weather-sunny", "stock_weather-night-clear",
						"stock_weather-fog", "stock_weather-storm",
						"stock_weather-few-clouds", "stock_weather-night-few-clouds",
						"stock_weather-cloudy", "stock_weather-showers"];
		this.smti = ["sunD", "sunN", "sunC", "sunA"];
		this.ci = ["c_bright", "c_color", "c_gamma", "c_display",
					"c_set", "c_about", "c_help", "c_applets",
//					"c_reload", "c_remove", "c_error", "c_power"];	// color icons array
					"c_reload", "c_remove", "c_error", "c_power",
					 "c_red", "c_green", "c_blue", "c_white"];	// color icons array
		this.gi = ["g_bright", "g_color", "g_gamma", "g_display",
					"g_set", "g_about", "g_help", "g_applets",
					"g_reload", "g_remove", "g_error", "g_power"];	// symbolic icons array (glyph)
		this.si = ["o_bright", "o_color", "o_gamma", "o_display",
					"o_set", "o_about", "o_help", "o_applets",
					"o_reload", "o_remove", "o_error", "o_power"];	// symbolic icons array (outline)
		this.truth = "application-javascript";
		this.iObj = [];
		this.iSizes = [ 12, 8, 12, 16, 22, 24, 32, 48, 64 ];
		this.disabled = _("disabled");			// monitor status
		this.disconn = _("not connected");	// monitor status
		this.TToutDisab = _("To change state you must first enable\nthis monitor in the Display panel");
		this.TToutDiscon = _("To change state you must first connect\na monitor to this output and then\nenable it in the Display panel");
		this.ttmod = _("Mode") + ": ";
		this.ttbri = _("Brightness") + ": ";
		this.ttgam = _("Gamma") + ": ";
		this.ttclr = _("Color temp") + ": ";
		this.ttcdy = _("(cloudy)");
		this.ttaon = _("(online)");
		this.ttaof = _("(offline)");
		this.smn_t = this.smn[this.screen_mode];
		this.brightness_custom = this.default_brightness_day;
		this.brightness_auto = this.maximum_brightness;
		this.colortemp_custom = this.default_colortemp_day;
		this.colortemp_auto = this.default_colortemp;
		this.gamma_custom = this.default_gamma_day;
		this.gamma_auto = this.default_gamma;
		this.auto_fmt = "ro-RO";
		this.auto_tz = "Europe/Bucharest";
		this.unlockTimer = null;
		this.loclast = "never";
		this.locupdate = false;
		this.lastdate = "";
		this.angle = 0;
/*=============================================================*/
		this._init_dependencies_satisfied();
		this.refresh_active_outputs();
		this.sun.dbg = this.dbgsw;
		this.setMode(this.screen_mode);
	},

	_get_applet_directory: function() {
		var type = Ext.Type["APPLET"]
		let dir = Ext.findExtensionDirectory(this._uuid, type.userDir, type.folder);
		return dir.get_path() + "/";
	},
/*==================== CHECK DEPENDENCIES =====================*/
	_init_dependencies_satisfied: function() {
		if (this._check_dependencies()) this._run_dependencies_satisfied();
	},

	_check_dependencies: function() {
		return this._check_xrandr() && this._check_randr();
	},

	_check_xrandr: function() {
		if (!(this._xrandr_available() && this._xrandr_version_satisfied())) {
			let dependencies = this._get_dependencies(this.xrandr_name, MinXrandrVersion);
			this._show_dialog_dependencies(dependencies);
			return false;
		}
		return true;
	},

	_xrandr_available: function() {
		let process = new ShellUtils.ShellOutputProcess(["which", this.xrandr_name]);
		let output = process.spawn_sync_and_get_output();
		return output.length > 0;
	},

	_xrandr_version_satisfied: function() {
		let lines = this._get_xrandr_version_lines();
		let line = this.get_line_or_empty_string(lines, this.xrandr_regex);
		return this._is_version_satisfied(line, MinXrandrVersion);
	},

	_get_xrandr_version_lines: function() {
		let process = new ShellUtils.ShellOutputProcess([this.xrandr_name, "--version"]);
		let output = process.spawn_sync_and_get_output();
		let lines = output.split('\n');
		return lines;
	},

	get_line_or_empty_string: function(lines, regex) {
		for (let line of lines) {
			if (regex.test(line)) return line;
		}
		return "";
	},

	_is_version_satisfied: function(line, min_version) {
		if (this.version_regex.test(line)) {
			let matches = line.match(this.version_regex);
			let version_match = matches[0];
			return parseFloat(version_match) >= min_version;
		}
		return false;
	},

	_get_dependencies: function(dependency, min_version) {
		return ">= " + dependency + " " + min_version;
	},

	_show_dialog_dependencies: function(dependencies) {
		let dialog_message = uuid + "\n\n" + _("The following packages were not found:") + "\n\n" +
							 dependencies + "\n\n" + _("Please install the above packages to use the applet");
		let dialog = new ModalDialog.NotifyDialog(dialog_message);
		dialog.open();
	},

	_check_randr: function() {
		if (!this._randr_version_satisfied()) {
			let dependencies = this._get_dependencies(this.randr_name, MinRandrVersion);
			this._show_dialog_dependencies(dependencies);
			return false;
		}
		return true;
	},

	_randr_version_satisfied: function() {
		let lines = this._get_xrandr_version_lines();
		let line = this.get_line_or_empty_string(lines, this.randr_regex);
		return this._is_version_satisfied(line, MinRandrVersion);
	},
/*====================== INITIALIZATIONS ======================*/
/*=============================================================*/
	_run_dependencies_satisfied: function () {
		this._init_layout();
		this._preload_values();
		this._bind_settings();
		this._connect_signals();
		this._init_values();
		this._init_more_values();
		this.sun = new Sundata.sun(this);
		this._init_screen_outputs();
		this._init_screen_output();
		this._init_context_menu();
		this._init_menu_sliders();
		this._init_gui();
		this._update_xrandr_startup();
		this.run();
	},
/*==================== INITIALIZING LAYOUT ====================*/
	_init_layout: function () {
		this._enable_horizontal_vertical_layout();
	},

	_enable_horizontal_vertical_layout: function() {
		if (this.is_vertical_layout_supported()) this._try_enable_horizontal_vertical_layout();
	},

	is_vertical_layout_supported: function() {
		return typeof this.setAllowedLayout === "function";
	},

	_try_enable_horizontal_vertical_layout: function() {
		try { this.setAllowedLayout(Applet.AllowedLayout.BOTH); }
		catch(e) { global.logError(aSig + "Error while enabling vertical and horizontal layout: " + e); }
	},
/*===================== BINDING SETTINGS ======================*/
	_bind_settings: function () {
		for (let [property_name, callback] of [
				["apply_asynchronously", null],
				["apply_startup", null],
				["apply_cycle", this.on_cycle_changed],
				["apply_every", this.on_cycle_changed],
				["update_scroll", null],
				["scroll_step_bright", null],
				["scroll_step_gamma", null],
				["scroll_step_color", null],
				["auto_day", this.on_auto_value_changed],
				["shift_day", this.on_auto_value_changed],
				["brightness_day", this.on_mode_value_changed],
				["gamma_day", this.on_mode_value_changed],
				["colortemp_day", this.on_mode_value_changed],
				["auto_night", this.on_auto_value_changed],
				["shift_night", this.on_auto_value_changed],
				["brightness_night", this.on_mode_value_changed],
				["gamma_night", this.on_mode_value_changed],
				["colortemp_night", this.on_mode_value_changed],
				["minimum_brightness", this.on_brightness_range_changed],
				["maximum_brightness", this.on_brightness_range_changed],
				["minimum_gamma", this.on_gamma_range_changed],
				["maximum_gamma", this.on_gamma_range_changed],
//				["minimum_colortemp", this.on_gamma_range_changed],	// altering these would
//				["maximum_colortemp", this.on_gamma_range_changed],	// screw up the colormap
				["cloudy_on", this.on_mode_value_changed],
				["cloudy_dark", this.on_mode_value_changed],
				["cloudy_color", this.on_mode_value_changed],
				["cloudy_gamma", this.on_mode_value_changed],
				["auto_start", this.on_auto_changed],
				["auto_mode", this.on_auto_changed],
				["options_type", this.on_options_type_changed],
				["debug", this.on_expert_debug_changed],
				["popup", this.on_expert_popup_changed],
				["tooltip", this.on_expert_tooltip_changed],
				["boldIt", this.on_expert_decor_changed],
				["decor", this.on_expert_decor_changed],
				["glyph", this.on_expert_decor_changed],
				["iSz", this.on_expert_decor_changed],
				["menu_bright", this.on_option_changed],
				["menu_color", this.on_option_changed],
				["menu_gamma", this.on_option_changed],
				["menu_rgb", this.on_option_changed],
				["locupdate", this.updateSundata],
				["loclast", null],
				["locinfo", null] ]) {
//				["last_values_string", null] ]) {
			this.settings.bind(property_name, property_name, callback);
		}
	},
/*==================== CONNECTING SIGNALS =====================*/
	_connect_signals: function() {
		try { this.actor.connect('scroll-event', Lang.bind(this, this.on_mouse_scroll)); }
		catch(e) { global.logError(aSig + "Error while connecting signals: " + e); }
		this.connect('MonitorsChanged', Lang.bind(this, this._init_screen_outputs)); // might need extra care
		global.settings.connect('changed::panel-edit-mode', Lang.bind(this, this.onPanelEditModeChanged));
	},
/*==================== INITIALIZING VALUES ====================*/
	_init_values: function () {
		this.load_last_values();
	},

	_preload_values: function() {
		this.settings.bind("last_values_string", "last_values_string", null);
		try {
			if (this.last_values_string.length > 0) {
				let row = Values.to_last_values_row(this.last_values_string);
				this.gamma_red = this._check(row.gamma_red, this.default_gamma);
				this.gamma_green = this._check(row.gamma_green, this.default_gamma);
				this.gamma_blue = this._check(row.gamma_blue, this.default_gamma);
				this.gamma = this._check(row.gamma, this.default_gamma);
				this.gamma_custom = this.gamma;
			}
		}
		catch(e) { global.logError(aSig + "Error while preloading gamma values: " + e); }
	},

	load_last_values: function() {
		try {
			if (this.last_values_string.length > 0) {
				let row = Values.to_last_values_row(this.last_values_string);
				this.screen_name = this._check(row.screen_name, "Screen 0");
				let output_indexes = row.output_indexes_string.split(this.output_indexes_separator);
				this.output_indexes = output_indexes.map(function(output_index) { return parseInt(output_index); });
				this.brightness = this._check(row.brightness, this.default_brightness_day);
//				this.gamma_red = this._check(row.gamma_red, this.default_gamma);
//				this.gamma_green = this._check(row.gamma_green, this.default_gamma);
//				this.gamma_blue = this._check(row.gamma_blue, this.default_gamma);
//				this.gamma = this._check(row.gamma, this.default_gamma);
//				this.gamma_custom = this.gamma;
				this.screen_mode = this._check(row.screen_mode, this.default_screen_mode);
				this.brightness_custom = this._check(row.brightness_custom, this.default_brightness_day);
				this.colortemp_custom = this._check(row.colortemp_custom, this.default_colortemp);
				if (this.auto_start) this.screen_mode = 3;
			}
		}
		catch(e) { global.logError(aSig + "Error while loading last values: " + e); }
	},

	_check: function (opt, val) {
		if (opt === "" || opt === null || opt === 'undefined' || opt === 'NaN') return val;
		return opt;
	},

	_init_more_values: function () {
		this.iObj = (this.decor) ? this.ci.slice() : (this.glyph ? this.gi.slice() : this.si.slice());
		this.icontype = (this.decor) ? St.IconType.FULLCOLOR : St.IconType.SYMBOLIC;
		this.iSize = this.iSizes[this.iSz];
		this.fSize = Math.round(5 + Math.sqrt(this.iSize)) + "pt";
		this.rise = Math.round(500*Math.sqrt(this.iSize));	// for tooltip globe/home symbols
		this.fStyle = "font-size: " + this.fSize + "; font-weight: " + (this.boldIt ? "bold" : "normal") + ";";
		this.simpleStyle = this.fStyle + " text-align: center;";
		this.default_tooltip = this.tthdr.format(_(this._meta.name), this._meta.version);
		this._initTTStyle(this.fSize);
		dbgsw = this.debug; this.dbgsw = this.debug;
	},

	_initTTStyle: function (fsz) {
		this.ttnorm = "text-align: center; font-weight: normal; font-size: " + fsz + ";";
		this.ttbold = "text-align: center; font-weight: bold; font-size: " + fsz + ";";
		this.ttstyle = (this.tooltip == true) ? this.ttbold : this.ttnorm;
		this._applet_tooltip._tooltip.set_style(this.ttstyle);
		this.set_applet_tooltip("Loading...");
	},
/*=============== INITIALIZING SCREEN & OUTPUTS ===============*/
	_init_screen_outputs: function () {
		let lines = this.list_screen_outputs();
		lines =  lines.split('\n');

		for (let i = 0; i < lines.length; ++i) {
			let line = lines[i];
			if (this._is_screen_line(line)) {
				let screen_name = this._parse_screen_name(line);
				let start_index = i + 1;
				let outputs = this._parse_outputs(lines, start_index);
				this.screen_outputs[screen_name] = outputs;
				i += outputs.length;
			}
		}
	},

	list_screen_outputs: function () {
		let process = new ShellUtils.ShellOutputProcess([this.xrandr_name, "--query"]);
		let output = process.spawn_sync_and_get_output();
		return output;
	},

	_is_screen_line: function (line) {
		return this.screen_regex.test(line);
	},

	_parse_screen_name: function (line) {
		let matches = line.match(this.screen_regex);
		let screen_name = matches[0];
		let last_character_index = screen_name.length - 1;
		screen_name = screen_name.substring(0, last_character_index);
		return screen_name;
	},

	_parse_outputs: function (lines, start_index) {
		let outputs = [];
		for (let i = start_index; i < lines.length; ++i) {
			let line = lines[i];
			if (this._is_output_line(line)) {
				let output = this._parse_output(line);
				outputs.push(output);
			} else if (this._is_screen_line(line)) break;
		}
		return outputs;
	},

	_is_output_line: function (line) {
		let strings = line.split(this.output_line_separator);
		return strings.length >= 2 &&
			(strings[this.output_status_index] == this.output_connected ||
			strings[this.output_status_index] == this.output_disconnected);
	},

	_parse_output: function (line) {
		let strings = line.split(this.output_line_separator);
		let output_name = strings[this.output_name_index];
		let is_connected = strings[this.output_status_index] == this.output_connected ? true : false;
		let is_enabled = this.isActiveOutput(output_name);
		let output = new Output(output_name, is_connected, is_enabled);
		return output;
	},

	_init_screen_output: function () {
		if (this.is_screen_output_default()) this.set_connected_screen();
	},

	is_screen_output_default: function () {
		return this.screen_name == this.default_screen_name &&
			this.output_indexes == this.default_output_indexes;
	},

	set_connected_screen: function () {
		for (let screen_name in this.screen_outputs) {
			let outputs = this.screen_outputs[screen_name];
			if (this.set_connected_outputs(outputs)) {
				this.screen_name = screen_name;
				return true;
			}
		}
		return false;
	},

	set_connected_outputs: function (outputs) {
		let found = false;
		for (let i = 0; i < outputs.length; ++i) {
			let output = outputs[i];
			if (output.is_connected) {
				this.output_indexes.push(i);
				found = true;
			}
		}
		return found;
	},
/*================= INITIALIZING CONTEXT MENU =================*/
	_init_context_menu: function () {
		this._applet_context_menu.removeAll();
		this._init_menu_item_screen();
		this._init_menu_item_outputs();
		this._init_menu_items_extra();
	},
//_initializing Screen submenu__
	_init_menu_item_screen: function () {
		let screen_names = this.get_screen_names();
		this.menu_item_screen = new AppletGui.RadioMenuItem(this, _("Screen"), screen_names);
		this.menu_item_screen.label.set_style(this.fStyle);
		this.menu_item_screen.set_callback_option_clicked(this, this.on_menu_item_screen_clicked);
		this.set_menu_item_screen_option();
		this._applet_context_menu.addMenuItem(this.menu_item_screen, this.menu_item_screen_position);
	},

	get_screen_names: function () {
		let screen_names = [];
		for (let screen_name in this.screen_outputs) {
			screen_names.push(screen_name);
		}
		return screen_names;
	},

	on_menu_item_screen_clicked: function (option_name, option_index) {
		if (this.screen_name != option_name) {
			this.screen_name = option_name;
			this.reload_outputs();
			this.reload_menu_item_outputs();
			this.update_xrandr();
		}
	},

	set_menu_item_screen_option: function () {
		if (this.is_screen_valid())
			this.menu_item_screen.set_active_option_name(this.screen_name);
	},

	is_screen_valid: function () {
		return this.dictionary_contains(this.screen_outputs, this.screen_name);
	},

	dictionary_contains: function (dictionary, key) {
		return key in dictionary;
	},

	reload_outputs: function () {
		let outputs = this.screen_outputs[this.screen_name];
		this.output_indexes = [];
		this.set_connected_outputs(outputs);
	},

	reload_menu_item_outputs: function () {
		let names = this.get_output_names();
		let checked = this.get_output_checked();
		this.menu_item_outputs.reload_options(names, checked);
	},

	get_output_names: function () {
		if (this.is_screen_valid()) {
			let outputs = this.screen_outputs[this.screen_name];
			return outputs.map(function(output) { return output.output_name; });
		}
		return [];
	},

	get_output_checked: function () {
		if (this.is_screen_valid()) return this.get_output_checked_valid();
		return [];
	},

	get_output_checked_valid: function () {
		let checked = [];
		let outputs = this.screen_outputs[this.screen_name];
		let index = 0;
		for (let i = 0; i < outputs.length; ++i) {
			outputs[i].is_enabled = this.isActiveOutput(outputs[i].output_name);
			let output_checked = index < this.output_indexes.length && i == this.output_indexes[index];
			checked[i] = index < this.output_indexes.length && i == this.output_indexes[index];
			if (this.testmode == 2) global.log("enable " + outputs[i].output_name + "=" + outputs[i].is_enabled + ", output_checked=" + output_checked + ", checked[" + i + "]" + checked[i]);
			if (output_checked) ++index;
		}
		return checked;
	},
//_initializing Outputs submenu_
	_init_menu_item_outputs: function () {
		this.menu_item_outputs = new AppletGui.CheckboxMenuItem(this, _("Outputs"));
		this.menu_item_outputs.label.set_style(this.fStyle);
		this.menu_item_outputs.set_callback_option_toggled(this, this.on_menu_item_outputs_toggled);
		this.reload_menu_item_outputs();
		this._applet_context_menu.addMenuItem(this.menu_item_outputs, this.menu_item_outputs_position);
	},

	on_menu_item_outputs_toggled: function (option_index, option_name, checked) {
		let array_index = this.output_indexes.indexOf(option_index);
		let contains = array_index > -1;
		if (contains) {
			this.array_remove(this.output_indexes, array_index);
		}
		else {
			this.array_insert(this.output_indexes, option_index);
			this.update_xrandr();
		}
	},

	array_remove: function (array, index) {
		array.splice(index, 1);
	},

	array_insert: function (array, number) {
		let index = this.find_insert_index(array, number);
		array.splice(index, 0, number);
	},

	find_insert_index: function (array, number) {
		let middle = 0;
		let lower = 0;
		let upper = array.length - 1;
		while (lower <= upper) {
			middle = Math.floor((lower + upper) / 2);
			if (number < array[middle]) upper = middle - 1;
			else if (number > array[middle]) lower = middle + 1;
			else return middle;
		}
		return lower == array.length ? array.length : middle;
	},
/*================= CONTEXT MENU EXTRA ITEMS ==================*/
	_init_menu_items_extra: function () {
//_this.context_menu_item_display
		this.display_menu =  new PopupMenu.PopupIconMenuItem(
			_("Open Display panel"), "video-display", this.icontype);
		this._addIcon(this.display_menu, this.iObj[3], this.iSize);
		this.display_menu.label.set_style(this.fStyle);
		this.display_menu.connect('activate', Lang.bind(this, function() {
			Util.spawnCommandLine(`cinnamon-settings display`); }));
		this._applet_context_menu.addMenuItem(this.display_menu);
//_menu items separator
		this._addSeparator(this._applet_context_menu);
//_this.context_menu_item_configure
		this.context_menu_item_configure =  new PopupMenu.PopupIconMenuItem(_("Settings..."),
			"system-run", this.icontype);
		this._addIcon(this.context_menu_item_configure, this.iObj[4], this.iSize);
		this.context_menu_item_configure.label.set_style(this.fStyle);
// Util.spawnCommandLine("xlet-settings applet " + this._uuid + " " + this.instance_id);
//		this.context_menu_item_configure.connect('activate', Lang.bind(this, this.configureApplet));
		this.context_menu_item_configure.connect('activate', () => this.openSettings({ x:30, y:30, width:480, height:640 }));
		this._applet_context_menu.addMenuItem(this.context_menu_item_configure);
//_this.context_menu_item_about
		this.context_menu_item_about =  new PopupMenu.PopupIconMenuItem(_("About..."),
			"dialog-information", this.icontype);
		this._addIcon(this.context_menu_item_about, this.iObj[5], this.iSize);
		this.context_menu_item_about.label.set_style(this.fStyle);
		this.context_menu_item_about.connect('activate', Lang.bind(this, this.openAbout));
		this._applet_context_menu.addMenuItem(this.context_menu_item_about);
//_this.context_menu_item_help
		this.help_menu =  new PopupMenu.PopupIconMenuItem(_("Help"),
			"dialog-question", this.icontype);
		this._addIcon(this.help_menu, this.iObj[6], this.iSize);
		this.help_menu.label.set_style(this.fStyle);
		this.help_menu.connect('activate', Lang.bind(this, function() {
			let txt = (this.mdAssoc()) ? this.mdFile : this.txtFile;
			if (this.file_exists(txt)) {
				try { Util.spawnCommandLine(`xdg-open ` + txt); }
				catch(e) { global.logError(aSig + "'xdg-open' cannot open the help file " + txt); }
			} else global.logError(aSig + "missing help file: " + txt);
		}));
		this._applet_context_menu.addMenuItem(this.help_menu);
//_menu items separator
		this._addSeparator(this._applet_context_menu);
//_initialize Troubleshooting submenu
		this._init_submenu_items_extra();
		this._applet_context_menu.addMenuItem(this.submenuTS);
//_menu items separator
		this._addSeparator(this._applet_context_menu);
//_this.context_menu_item_remove
		this.context_menu_item_remove =  new PopupMenu.PopupIconMenuItem(_("Disable '%s'").
			format(this._(this._meta.name)), "edit-delete", this.icontype);
		this._addIcon(this.context_menu_item_remove, this.iObj[9], this.iSize);
		this.context_menu_item_remove.label.set_style(this.fStyle);
		this.context_menu_item_remove.connect('activate', Lang.bind(this, function(actor, event) {
			if (Clutter.ModifierType.CONTROL_MASK & Cinnamon.get_event_state(event)) {
				AppletManager._removeAppletFromPanel(this._uuid, this.instance_id);
			} else {
				let dialog = new ModalDialog.ConfirmDialog(
					_("Are you sure you want to remove %s?").format(this._meta.name),
					() => AppletManager._removeAppletFromPanel(this._uuid, this.instance_id)
				);
				dialog.open();
			}
		}));
		this._applet_context_menu.addMenuItem(this.context_menu_item_remove);
//_connect signals to open submenu by default in Debug mode
		if (this.dbgsw) this._applet_context_menu.connect("open-state-changed",
			Lang.bind(this, this._openSubmenu));
//_connect signals to close submenu when leaving it (unfortunately they crash Cinnamon !!!)
/*		this.help_menu.actor.connect("enter-event", Lang.bind(this, this._closeSubmenu));
		this.help_menu.actor.connect("key-focus-in", Lang.bind(this, this._closeSubmenu));
		this.context_menu_item_remove.actor.connect("enter-event", Lang.bind(this, this._closeSubmenu));
*/	},
/*============= CONTEXT MENU EXTRA ITEMS SUBMENU ==============*/
	_init_submenu_items_extra: function () {
		this.submenuTS = new PopupMenu.PopupSubMenuMenuItem(_("Troubleshooting"));
		this.submenuTS.label.set_style(this.fStyle);
//_forced reload items for this applet and Cinnamon
		if (this.testmode) {
			this.reappF_menu = new PopupMenu.PopupIconMenuItem(
				_("Reload applet [forced]"), "reload", this.icontype);
			this._addIcon(this.reappF_menu, this.iObj[8], this.iSize);
			this.reappF_menu.connect('activate', Lang.bind(this, function() {
				Ext.reloadExtension(uuid, Ext.Type.APPLET);
			}));
			this.submenuTS.menu.addMenuItem(this.reappF_menu);
			this.recinnF_menu = new PopupMenu.PopupIconMenuItem(
				_("Reload Cinnamon [forced]"), "cinnamon", this.icontype);
			this.recinnF_menu.connect('activate', Lang.bind(this, function() {
				global.reexec_self();
			}));
			this.submenuTS.menu.addMenuItem(this.recinnF_menu);
			this._addSeparator(this.submenuTS.menu);
		}
//_this.context_menu_item_applets
		this.applets_menu =  new PopupMenu.PopupIconMenuItem(_("Open Applets panel"),
			"cs-applets", this.icontype);
		this._addIcon(this.applets_menu, this.iObj[7], this.iSize);
		this.applets_menu.label.set_style(this.fStyle);
		this.applets_menu.connect('activate', Lang.bind(this, function() {
			Util.spawnCommandLine(`cinnamon-settings applets`); }));
		this.submenuTS.menu.addMenuItem(this.applets_menu);
//_this.context_menu_item_reloadApp
		this.reapp_menu =  new PopupMenu.PopupIconMenuItem(_("Reload '%s'").
			format(this._(this._meta.name)), "reload_page", this.icontype);
		this._addIcon(this.reapp_menu, this.iObj[8], this.iSize);
		this.reapp_menu.label.set_style(this.fStyle);
		this.reapp_menu.connect('activate', Lang.bind(this, function() {
			let msg = _("Are you sure you want to reload '") + this._meta.name + "' ?";
			let dialog = new ModalDialog.ConfirmDialog(msg, Lang.bind(this, function() {
				Main.warningNotify(this._meta.name +  _(" has been reloaded."),
					_("If reloading the applet doesn't run the correct code,\n" +
					"you should reload Cinnamon itself. It's a known bug."));
				Ext.reloadExtension(uuid, Ext.Type.APPLET);
			}));
			dialog.open();
		}));
		this.submenuTS.menu.addMenuItem(this.reapp_menu);
//_this.context_menu_item_reloadCinn
		this.recinn_menu =  new PopupMenu.PopupIconMenuItem(_("Reload Cinnamon"),
			"cinnamon", this.icontype);
		if (this.iSize) this.recinn_menu._icon.set_icon_size(this.iSize);
		this.recinn_menu.label.set_style(this.fStyle);
		this.recinn_menu.connect('activate', Lang.bind(this, function() {
			let msg = _("You are about to reload the Cinnamon desktop.\n\n" +
				"Please wait until the operation is complete," +
				"\nit may take a few seconds with many x-lets active.\n\n" +
				"Proceed with reloading Cinnamon?");
			let dialog = new ModalDialog.ConfirmDialog(msg, function() {
				global.reexec_self(); });
			dialog.open();
		}));
		this.submenuTS.menu.addMenuItem(this.recinn_menu);
		this.submenuTS.actor.connect("enter-event", Lang.bind(this, this._openSubmenu));
		this.submenuTS.actor.connect("key-focus-in", Lang.bind(this, this._openSubmenu));
	},

	_openSubmenu: function() {
		if (!this.submenuTS.menu.isOpen) this.submenuTS.menu.open();
	},

	_closeSubmenu: function() {
		if (this.submenuTS.menu.isOpen) this.submenuTS.menu.close();
	},

	_addSeparator: function(menu) {
		let params = {margin: 1, width: 70, fPath: this.colorsep_path};
		menu.addMenuItem(new CtrlPlus.PopupSeparatorPlusMenuItem(this.decor ? params : {margin: 0}));
	},

	_addIcon: function (label, gIcon, iSize) {
		if (iSize) label._icon.set_icon_size(iSize);
		if (gIcon) label._icon.set_icon_name(gIcon);
	},
/*========== INITIALIZE SLIDERS MENU (CONTROLS GUI) ===========*/
	_init_menu_sliders: function () {
		this.menu_sliders = new AppletGui.MenuSliders(this, this.panel_orientation);
	},
/*=================== INITIALIZE PANEL ICON ===================*/
	_init_gui: function () {
		this.set_gui_icon(this.screen_mode);
	},
//_setting panel icon___________
	set_gui_icon: function (mode) {
		this.set_applet_icon_name(this.smti[mode])
	},

	remove_file_schema: function (path) {
		return path.replace(this.file_schema, "");
	},

	replace_tilde_with_home_directory: function (path) {
		let home_directory = GLib.get_home_dir();
		return path.replace(this.home_shortcut, home_directory);
	},

	file_exists: function (path) {
		return GLib.file_test(path, GLib.FileTest.EXISTS);
	},
/*================== UPDATE SCREEN AT STARTUP =================*/
	_update_xrandr_startup: function () {
		if (this.apply_startup) this.update_xrandr();
	},

	update_xrandr: function () {
		for (let output_index of this.output_indexes) {
			// Avoid xrandr errors 'Need crtc to set gamma on' on inactive displays
			let obj = this.checkConnected(this._get_output_parameter(output_index));
			if (!obj) continue;
			if (this.is_screen_output_valid(output_index)) this.update_xrandr_output(output_index);
		}
	},
//_this one checks if display is actually connected and active;
//_avoids repetitive crtc error messages in log for laptops
//_when lid is closed and mode is set to Auto
	checkConnected: function(id) {
		// take a chance rather than making whole applet useless
		if (!this.RRscreen) return true;
		let config = CDesk.RRConfig.new_current(this.RRscreen);
		let infos = config.get_outputs(); // array of RROutputInfo
		for (let item of infos) {
			let sname = item.get_name();
			if (sname != id) continue;
			let obj = this.RRscreen.get_output_by_name(sname);
			if (!obj || !obj.is_connected()) return false;
			if (item.is_active()) return true;
		}
		return false;
	},

	is_screen_output_valid: function (output_index) {
		return this.is_screen_valid() && this.is_output_valid(output_index);
	},

	is_output_valid: function (output_index) {
		return output_index >= 0 &&
			output_index < this.screen_outputs[this.screen_name].length &&
			this.screen_outputs[this.screen_name][output_index].is_enabled;
	},

	isActiveOutput: function (i) {
		let name = (typeof i == 'string') ?
			i : this.screen_outputs[this.screen_name][i].output_name;
		let process = new ShellUtils.ShellOutputProcess([this.xrandr_name, "--listactivemonitors"]);
		let output = process.spawn_sync_and_get_output();
		return new RegExp(name, 'm').test(output);
	},

	update_xrandr_output: function (output_index) {
		let argv = this.get_xrandr_argv(output_index);
		this.spawn_xrandr_process(argv);
	},

	get_xrandr_argv: function (output_index) {
		let screen_parameter = this._get_screen_parameter();
		let output_parameter = this._get_output_parameter(output_index);
		let brightness_parameter = this._get_brightness_parameter();
		let gamma_parameter = this._get_gamma_parameter();

		let argv = [this.xrandr_name, "--screen", screen_parameter,
									  "--output", output_parameter,
									  "--brightness", brightness_parameter,
									  "--gamma", gamma_parameter];
		if (this.dbgsw) global.log(SEP + "get_xrandr_argv(): argv=" + argv);
		return argv;
	},

	_get_screen_parameter: function() {
		let matches = this.number_regex.test(this.screen_name) ?
			this.screen_name.match(this.number_regex) : ["0"];
		let screen_index = matches.length > 0 ? matches[0] : "0";
		let parameter = screen_index.toString();
		return parameter;
	},

	_get_output_parameter: function(output_index) {
		let output = this.get_active_output(output_index);
		let parameter = output.output_name;
		return parameter;
	},

	get_active_output: function (output_index) {
		let outputs = this.screen_outputs[this.screen_name];
		let output = outputs[output_index];
		return output;
	},

	_get_brightness_parameter: function() {
		return this._get_scaled_parameter(this.brightness);
	},

	_get_scaled_parameter: function(number) {
		number = number / 100;
		let parameter = number.toString();
		return parameter;
	},

	_get_gamma_parameter: function() {
		let parameter_red = this._get_scaled_parameter(this.gamma_red);
		let parameter_green = this._get_scaled_parameter(this.gamma_green);
		let parameter_blue = this._get_scaled_parameter(this.gamma_blue);
		let parameter = parameter_red + this.gamma_separator + parameter_green + this.gamma_separator + parameter_blue;
		return parameter;
	},

	spawn_xrandr_process: function (argv) {
		if (this.apply_asynchronously) this.spawn_xrandr_process_async(argv);
		else this.spawn_xrandr_process_sync(argv);
	},

	spawn_xrandr_process_async: function (argv) {
		try {
			let xrandr_process = new ShellUtils.BackgroundProcess(argv, true);
			xrandr_process.set_callback_process_finished(this, this.on_xrandr_async_finished);
			xrandr_process.spawn_async();
		}
		catch(e) { global.logError(aSig + "Error while spawning asynchronously xrandr process: " + e.message); }
	},

	on_xrandr_async_finished: function (xrandr_process, pid, status) {
		let error = xrandr_process.get_standard_error_content();
		if (error.length > 0) {
			let error_message = "Error while updating brightness and gamma asynchronously: " + error;
			this.log_process_error(error_message, xrandr_process.command_argv);
		}
	},

	log_process_error: function(error_message, argv) {
		global.logError(aSig + error_message + "Command line arguments: " + argv);
	},

	spawn_xrandr_process_sync: function (argv) {
		let xrandr_process = new ShellUtils.ShellOutputProcess(argv);
		let error = xrandr_process.spawn_sync_and_get_error();
		if (error.length > 0) {
			let error_message = "Error while updating brightness and gamma synchronously: " + error;
			this.log_process_error(error_message, xrandr_process.command_argv);
		}
	},

	run: function () {
		this._run_apply_values_running();
	},

	_run_apply_values_running: function () {
		if (this.is_running) this._apply_values();
	},

	_apply_values: function () {
		if (this.apply_cycle && this.apply_every > 0) {
			this.update_xrandr();
			Mainloop.timeout_add(1000 * this.apply_every, Lang.bind(this, this._run_apply_values_running));
		}
	},

	save_last_values: function() {
		try {
			let output_indexes_string = this.output_indexes.join(this.output_indexes_separator);
			let row = new Values.LastValuesRow(this.screen_name, output_indexes_string, this.brightness,
						this.gamma_red, this.gamma_green, this.gamma_blue, this.colortemp_custom,
						this.screen_mode, this.brightness_custom, this.gamma_custom);
			this.last_values_string = Values.to_csv_string(row);
			if (dbgsw) global.log(SEP + "save_last_values(): saved values: " + this.last_values_string);
		}
		catch(e) { global.logError(aSig + "Error while saving last values: " + e); }
	},
/*================== REFRESH ACTIVE OUTPUTS ===================*/
	refresh_active_outputs: function() {
		this.tts = "";
		for (let output_index of this.output_indexes) {
			if (this.is_screen_output_valid(output_index)) {
				this.tts += this.get_active_output(output_index).output_name + " ";
			}
		}
		return this.tts;
	},
/*========================= CALLBACKS =========================*/
//_
	on_gui_icon_changed: function () {
		this.set_gui_icon(this.screen_mode);
	},
//_
	on_orientation_changed: function(orientation) {
		this.orientation = orientation;
		this.isHorizontal = !(this.orientation == St.Side.LEFT || this.orientation == St.Side.RIGHT);
	},

	on_brightness_range_changed: function () {
		let value = this.get_range_value(this.minimum_brightness, this.maximum_brightness, this.brightness);
		let outside = this.brightness != value;
		this.brightness = value;
		this.menu_sliders.update_items_brightness();
		let gMark = this.menu_sliders.getMark(this.minimum_brightness, this.maximum_brightness, 100);
		gMark = gMark < 1.0 ? gMark : 0;
		this.menu_sliders.sb.set_mark(gMark);
		this.update_brightness_active(outside);
		return outside;
	},

	on_gamma_range_changed: function () {
		let value = this.get_range_value(this.minimum_gamma, this.maximum_gamma, this.gamma);
		let outside = false;
		outside = this.on_gamma_red_range_changed() || outside;
		outside = this.on_gamma_green_range_changed() || outside;
		outside = this.on_gamma_blue_range_changed() || outside;
		this.menu_sliders.update_items_gamma();
		this.update_gamma_active(outside);
		let gMark = this.menu_sliders.getMark(this.minimum_gamma, this.maximum_gamma, 100);
		gMark = gMark < 1.0 ? gMark : 0;
		this.menu_sliders.sg.set_mark(gMark);
		this.menu_sliders.gr.set_mark(gMark);
		this.menu_sliders.gg.set_mark(gMark);
		this.menu_sliders.gb.set_mark(gMark);
		return outside;
	},

	on_gamma_red_range_changed: function () {
		let value = this.get_range_value(this.minimum_gamma, this.maximum_gamma, this.gamma_red);
		let outside = this.gamma_red != value;
		this.gamma_red = value;
		this.menu_sliders.update_items_gamma_red();
		return outside;
	},

	on_gamma_green_range_changed: function () {
		let value = this.get_range_value(this.minimum_gamma, this.maximum_gamma, this.gamma_green);
		let outside = this.gamma_green != value;
		this.gamma_green = value;
		this.menu_sliders.update_items_gamma_green();
		return outside;
	},

	on_gamma_blue_range_changed: function () {
		let value = this.get_range_value(this.minimum_gamma, this.maximum_gamma, this.gamma_blue);
		let outside = this.gamma_blue != value;
		this.gamma_blue = value;
		this.menu_sliders.update_items_gamma_blue();
		return outside;
	},

	on_colortemp_range_changed: function () {
		let value = this.get_range_value(this.minimum_colortemp, this.maximum_colortemp, this.colortemp);
		let outside = this.colortemp != value;
		this.colortemp = value;
		this.menu_sliders.update_items_colortemp();
		return outside;
	},

	on_cycle_changed: function () {
		this.run();
	},

	on_auto_changed: function () {
		if (this.dbgsw) global.log("auto_mode changed to " + this.auto_mode);
		this.max_mode = (this.auto_mode > OPTA.NO) ? 3 : 2;
		if (this.max_mode == 2 && this.screen_mode == 3) {
			this.screen_mode = 2;
			this.setMode(this.screen_mode);
		} else if (this.max_mode == 3 && this.screen_mode != 3 && this.auto_mode > OPTA.NO) {
			this.screen_mode = 3;
			this.setMode(this.screen_mode);
		}
		if (this.screen_mode == 3) this.sun.timer((this.auto_mode > OPTA.NO && !this.sun.timer) ? true : false);
		else this.sun.timer(false);
	},

	on_auto_value_changed: function () {
		if (this.screen_mode != 3) return;	// not Auto mode currently, don't bother
//_recalculate day/night/twilight thresholds and apply values
		this.sun.getDayslice();
	},

	on_mode_value_changed: function () {
		let b = 0; let c = 0; let g = 0;
		if (this.cloudy_on && this.cloudy) {
			b = this.cloudy_dark; c = this.cloudy_color; g = this.cloudy_gamma;
		}
		if (this.screen_mode == 3) {	// Auto mode, what should we do?
			let t = this.sun._localTime();
			let dt = this.sun._localTime(new Date().setHours(this.auto_day));
			let nt = this.sun._localTime(new Date().setHours(this.auto_night));
			if ((this.auto_mode == OPTA.OL && this.sun.cds == 1) ||
				(this.auto_mode == OPTA.OF && (t >= nt || t< dt))) {	// night
				this.brightness = this.brightness_night - b;
				this.gamma = this.gamma_night - g;
				this.colortemp = this.colortemp_night - c;
			}
			else if ((this.auto_mode == OPTA.OL && this.sun.cds == 4) ||
				(this.auto_mode == OPTA.OF && (t >= dt || t< nt))) {	// day
				this.brightness = this.brightness_day - b;
				this.gamma = this.gamma_day - g;
				this.colortemp = this.colortemp_day - c;
			} else {	// calculate and apply the percentage based on new values, accounting for a cloudy mode in effect
			this.sun.getDayslice();
			}
		}
		else if (this.screen_mode == 2) return;	// Custom mode, don't bother if values changed
		else if (this.screen_mode == 0) {
			this.brightness = this.brightness_day - b;
			this.gamma = this.gamma_day - g;
			this.colortemp = this.colortemp_day - c;
		}
		else if (this.screen_mode == 1) {
			this.brightness = this.brightness_night - b;
			this.gamma = this.gamma_night - g;
			this.colortemp = this.colortemp_night - c;
		}
		this.updateAll();
	},
/*======= CHOOSE SLIDER DISPLAY MODE: NONE/SELECTED/ALL =======*/
	on_options_type_changed: function (val) {
	try {	if (this.dbgsw) global.log(SEP + "on_options_type_changed(): to " + val); }
	catch(e) { global.log(SEP + val); }
		if (this.options_type == OPTS.ALL) {
/*			this.menu_bright = true;
			this.menu_gamma = true;
			this.menu_color = true;
			this.menu_rgb = true;
			this.settings.setValue("menu_bright", true);
			this.settings.setValue("menu_gamma", true);
			this.settings.setValue("menu_color", true);
			this.settings.setValue("menu_rgb", true);
*/		if (this.dbgsw) global.log("on_options_type_changed(): changed menu_* values to true");
		}
		else if (this.options_type == OPTS.NONE) {
/*			this.menu_bright = false;
			this.menu_gamma = false;
			this.menu_color = false;
			this.menu_rgb = false;
			this.settings.setValue("menu_bright", false);
			this.settings.setValue("menu_gamma", false);
			this.settings.setValue("menu_color", false);
			this.settings.setValue("menu_rgb", false);
*/		if (this.dbgsw) global.log("on_options_type_changed(): changed menu_* values to false");
		}
	},
/*================== SELECT AVAILABLE SLIDERS =================*/
	on_option_changed: function (val) {
		if (this.dbgsw) global.log(SEP + "on_option_changed(): changed to " + val +
			", this.menu_bright=" + this.menu_bright + ", this.menu_color=" + this.menu_color +
			", this.menu_gamma=" + this.menu_gamma + ", this.menu_rgb=" + this.menu_rgb);
/*		let m1 = this.menu_bright || this.menu_color || this.menu_gamma || this.menu_rgb;
		let m2 = this.menu_bright && this.menu_color && this.menu_gamma && this.menu_rgb;
		if (this.options_type == OPTS.ALL && m2 == false)
			this.settings.setValue("options_type", OPTS.SEL);
		else if (this.options_type == OPTS.NONE && m1 == true)
			this.settings.setValue("options_type", OPTS.SEL);
*/		this._init_menu_sliders();
		this.menu_sliders.open();
		this._hideMenuTimer = Mainloop.timeout_add(4500, Lang.bind(this, this.hideMenu));
		if (this.dbgsw) global.log("on_option_changed(): reinitialized menu_sliders\noptions_type=" +
			options_type + ", m1=" + m1 + ", m2=" + m2);
	},
/*======================= EXTRA SETTINGS ======================*/
	on_expert_debug_changed: function() {
		dbgsw = this.debug; this.dbgsw = this.debug;
		try { this.sun.dbg = this.debug; } catch(e) {  }
		try { this.menu_sliders.dbg = this.debug; } catch(e) {  }
	},

	on_expert_popup_changed: function() {
		var notif_txt = this.ttmod + "<b>" + _(this.smn_t) + "</b> " +
			_("for") + " " + this.screen_name + ": " + this.tts;
		this._notifyMessage(this.smi[this.screen_mode], notif_txt);
	},

	on_expert_tooltip_changed: function() {
		this.ttstyle = (this.tooltip == true) ? this.ttbold : this.ttnorm;
		this._applet_tooltip._tooltip.set_style(this.ttstyle);
		this._applet_tooltip._tooltip.show();
		this._applet_tooltip._tooltip.visible = true;
		this._hideTimer = Mainloop.timeout_add(4500, Lang.bind(this, this.hideTip));
	},

	on_expert_decor_changed: function() {
		this.icontype = (this.decor) ? St.IconType.FULLCOLOR : St.IconType.SYMBOLIC;
		this.iSize = this.iSizes[this.iSz];
		this.fSize = Math.round(5 + Math.sqrt(this.iSize)) + "pt";
		this.fStyle = "font-size: " + this.fSize + "; font-weight: " + (this.boldIt ? "bold" : "normal") + ";";
		this.simpleStyle = this.fStyle + " text-align: center;";
		this.rise = Math.round(500*Math.sqrt(this.iSize));	// for tooltip globe/home symbols
		this.iObj = (this.decor) ? this.ci.slice() : (this.glyph ? this.gi.slice() : this.si.slice());
		this._initTTStyle(this.fSize);
//_reinitialize all menus
		this._init_context_menu();
		this._init_menu_sliders();
		this.menu_sliders.open();
		this._hideMenuTimer = Mainloop.timeout_add(4500, Lang.bind(this, this.hideMenu));
	},

	hideTip: function() {
		Mainloop.source_remove(this._hideTimer);
		this._hideTimer = null;
		this._applet_tooltip._tooltip.hide();
		return false;
	},

	hideMenu: function() {
		Mainloop.source_remove(this._hideMenuTimer);
		this._hideMenuTimer = null;
		this.menu_sliders.close();
		this._applet_context_menu.open();
		this._hideCtxMenuTimer = Mainloop.timeout_add(4500, Lang.bind(this, this.hideCtxMenu));
		return false;
	},

	hideCtxMenu: function() {
		Mainloop.source_remove(this._hideCtxMenuTimer);
		this._hideCtxMenuTimer = null;
		this._applet_context_menu.close();
		return false;
	},
/*====================== HELPER FUNCTIONS =====================*/
	get_range_value: function (min_value, max_value, value) {
		return Math.max(Math.min(value, max_value), min_value);
//		return value;
	},

	update_xrandr_outside: function (outside) {
		if (outside) this.update_xrandr();
	},
/*==================== APPLY UPDATED VALUES ===================*/
	update_brightness_active: function (outside) {
		if (this.is_brightness_active()) this.update_xrandr_outside(outside);
	},

	is_brightness_active: function () {
		if (this.dbgsw) global.log(SEP + "is_brightness_active(): options_type=" + this.options_type +
			" menu_bright " + this.menu_bright);
		return this.options_type == OPTS.ALL ||
			(this.options_type == OPTS.SEL && this.menu_bright == true);
	},

	update_gamma_active: function (outside) {
		if (this.is_gamma_active()) this.update_xrandr_outside(outside);
	},

	is_gamma_active: function () {
		if (this.dbgsw) global.log(SEP + "is_gamma_active(): options_type=" + this.options_type +
			" menu_gamma " + this.menu_gamma);
		return this.options_type == OPTS.ALL ||
			(this.options_type == OPTS.SEL && this.menu_gamma == true);
	},

	update_colortemp_active: function (outside) {
		if (this.is_colortemp_active()) this.update_xrandr_outside(outside);
	},

	is_colortemp_active: function () {
		if (this.dbgsw) global.log(SEP + "is_colortemp_active(): options_type=" + this.options_type +
			" menu_color " + this.menu_color);
		return this.options_type == OPTS.ALL ||
			(this.options_type == OPTS.SEL && this.menu_color == true);
	},

	update_rgb_active: function (outside) {
		if (this.is_rgb_active()) this.update_xrandr_outside(outside);
	},

	is_rgb_active: function () {
		if (this.dbgsw) global.log(SEP + "is_rgb_active(): options_type=" + this.options_type +
			" menu_gamma " + this.menu_gamma + " menu_rgb " + this.menu_rgb);
		return this.options_type == OPTS.ALL ||
			(this.options_type == OPTS.SEL && this.menu_gamma == true && this.menu_rgb == true);
	},
/*====================== UPDATE SCREEN ========================*/
	update_brightness: function(value) {
		this.brightness = value;
		if (this.screen_mode == 2) this.brightness_custom = this.brightness;
		this.update_xrandr();
	},

	update_gamma_red: function(value) {
		this.gamma_red = value;
		this.update_xrandr();
	},

	update_gamma_green: function(value) {
		this.gamma_green = value;
		this.update_xrandr();
	},

	update_gamma_blue: function(value) {
		this.gamma_blue = value;
		this.update_xrandr();
	},

	update_colortemp: function(value) {
		this.colortemp = value;
		if (this.screen_mode == 2) this.colortemp_custom = this.colortemp;
		this.update_xrandr();
	},

	update_gamma: function(value) {
		this.gamma = value;
		if (this.screen_mode == 2) this.gamma_custom = this.gamma;
		this.update_xrandr();
	},
/*======================== SCROLL EVENTS ======================*/
	on_mouse_scroll: function(actor, event) {
		if (this.update_scroll) {
			let mod1 = event.get_state() & 1;	// test for <Shift> down
			let mod2 = event.get_state() & 4;	// test for <Ctrl> down
			let mod3 = event.get_state() & 64;	// test for <Super/Win> down
			let direction = event.get_scroll_direction();
			if (direction == Clutter.ScrollDirection.SMOOTH) return;
			if (this.dbgsw) global.log(SEP + "on_mouse_scroll(): scroll " + (direction ? "down" : "up") +
				" with " + (!(mod1 || mod2 || mod3) ? "no modifiers" : (mod1 ? "<Shift> " : "") +
				(mod2 ? "<Ctrl> " : "") + (mod3 ? "<Win> " : "")));
			if (!mod1 && !mod2 && !mod3)
				this.scroll_brightness(direction);
			else if (mod1 && !mod2 && !mod3)
				this.scroll_gamma(direction);
			else if (!mod1 && mod2 && !mod3)
				this.scroll_colortemp(direction);
// Pondering whether to add separate icons for each active monitor (how many could they be?)
// or scroll through monitors using <Super/Win>+Wheel.
// For the latter we need to supress keypress event propagation, otherwise it opens main menu.
		}
	},

	scroll_brightness: function(dir) {
		let value = (dir == Clutter.ScrollDirection.UP) ? this.brightness + this.scroll_step_bright :
		this.brightness - this.scroll_step_bright;
		this.update_brightness_scroll(value);
	},

	scroll_gamma: function(dir) {
		let value = (dir == Clutter.ScrollDirection.UP) ? this.gamma + this.scroll_step_gamma :
		this.gamma - this.scroll_step_gamma;
		value = this.get_range_value(this.minimum_gamma, this.maximum_gamma, value);
		if (value != this.gamma) {
			this.gamma = value;
			this.menu_sliders.gamma2RGB(this.gamma);
			this.menu_sliders.update_items_gamma();
			this.menu_sliders.update_items_rgb();
			this.update_gamma(value);
//			this.set_applet_tooltip(this.update_tooltip_values());
			this.update_tooltip_values();
			this._applet_tooltip._tooltip.show();
		}
	},

	scroll_colortemp: function(dir) {
		let value = (dir == Clutter.ScrollDirection.UP) ? this.colortemp + this.scroll_step_color :
		this.colortemp - this.scroll_step_color;
		value = this.get_range_value(this.minimum_colortemp, this.maximum_colortemp, value);
		if (value != this.colortemp) {
			this.colortemp = value;
			this.menu_sliders.updateCT(value, this.gamma);
			this.menu_sliders.update_items_colortemp();
			this.menu_sliders.update_items_rgb();
			this.update_colortemp(value);
//			this.set_applet_tooltip(this.update_tooltip_values());
			this.update_tooltip_values();
			this._applet_tooltip._tooltip.show();
		}
	},

	update_brightness_scroll: function(value) {
		value = this.get_range_value(this.minimum_brightness, this.maximum_brightness, value);
		if (value != this.brightness) {
			this.update_brightness(value);
			this.menu_sliders.update_items_brightness();
//			this.set_applet_tooltip(this.update_tooltip_values());
			this.update_tooltip_values();
			this._applet_tooltip._tooltip.show();
		}
	},
/*========================= OVERRIDES =========================*/
	onPanelEditModeChanged: function() {
		if (global.settings.get_boolean("panel-edit-mode")) {
			this.actor.set_track_hover(true);
		} else {
			this.actor.set_hover(false);
			this.actor.set_track_hover(false);
		}
	},
// common cleanup
	_cleanup: function() {
		if (this.checker) this.checker._cleanup();
		this.sun.timer(false);
		this.sun.secTimer(false);
	},

// cleanup on applet reloaded
	on_applet_reloaded: function() {
		this._cleanup();
	},
//_disable/remove from panel____
	on_applet_removed_from_panel: function() {
		this._cleanup();
		this.save_last_values();
		this.settings.finalize();
		if (typeof settingsWindow == 'object') {
			settingsWindow.delete(global.display.get_current_time());
		}
		this.is_running = false;
	},
//_click applet tray icon_______
	on_applet_clicked: function(event) {
		let mod1 = event.get_state() & 1;	// test for <Shift> down
		let mod2 = event.get_state() & 4;	// test for <Ctrl> down
		if (!mod1 && !mod2) this.menu_sliders.toggle();
		else if (mod2 && this.cloudy_on) {
			this.cloudy = !this.cloudy;
			if (this.cloudy) {
				this.update_brightness_scroll(this.brightness - this.cloudy_dark);
				this.update_gamma(this.gamma - this.cloudy_gamma);
				this.update_colortemp(this.colortemp - this.cloudy_color);
			} else {
				this.update_brightness_scroll(this.brightness + this.cloudy_dark);
				this.update_gamma(this.gamma + this.cloudy_gamma);
				this.update_colortemp(this.colortemp + this.cloudy_color);
			}
		}
		this.updateAll();
		Mainloop.timeout_add(50, () => this.showTip());
	},

	showTip: function() {
		this._applet_tooltip._tooltip.show();
		this._applet_tooltip._tooltip.visible = true;
		return false;
	},
//_middle-click tray icon_______
	on_applet_middle_clicked: function(event) {
		let mod1 = event.get_state() & 1;	// test for <Shift> down
		let mod2 = event.get_state() & 4;	// test for <Ctrl> down
		let last_mode = this.screen_mode;
		if (mod2) this.screen_mode = this.screen_mode > 0 ? --this.screen_mode : this.max_mode;
		else if (!mod1 && !mod2) this.screen_mode = this.screen_mode < this.max_mode ? ++this.screen_mode : 0;
		if (this.screen_mode < 3 && this.sun.timeout) this.sun.timer(false);
		this.setMode(this.screen_mode, last_mode);
		Mainloop.timeout_add(250, () => this.showTip());
	},
/*==================== ADDITIONAL FUNCTIONS ===================*/
	setMode: function(mode, last) {
		let b = 0; let c = 0; let g = 0;
		if (this.cloudy_on && this.cloudy) {
			b = this.cloudy_dark;
			c = this.cloudy_color;
			g = this.cloudy_gamma;
		}
		if (mode == 0) {
			this.brightness = this.brightness_day - b;
			this.colortemp = this.colortemp_day - c;
			this.gamma = this.gamma_day - g;
		}
		else if (mode == 1) {
			this.brightness = this.brightness_night - b;
			this.colortemp = this.colortemp_night - c;
			this.gamma = this.gamma_night - g;
		}
		else if (mode == 2) {
			this.brightness = this.brightness_custom - b;
			this.colortemp = this.colortemp_custom - c;
			this.gamma = this.gamma_custom - g;
		}
		else if (mode == 3) {
			if (last == 2) {
				this.brightness_custom = this.brightness + b;
				this.colortemp_custom = this.colortemp + c;
				this.gamma_custom = this.gamma + g;
			}
			if (!this.sun.timeout) this.sun.timer(true);
			if (this.sun.cds == 1) {
				this.brightness = this.brightness_night - b;
				this.colortemp = this.colortemp_night - c;
				this.gamma = this.gamma_night - g;
			}
			else if (this.sun.cds == 4) {
				this.brightness = this.brightness_day - b;
				this.colortemp = this.colortemp_day - c;
				this.gamma = this.gamma_day - g;
			}
			if (this.dbgsw) global.log(SEP + "setMode(): screen_mode 3, auto_mode=" +
				this.auto_mode + ", dayslice=" + this.sun.cds + " last=" + this.sun.lds);
		}
		this.set_gui_icon(this.screen_mode);
		this.updateAll();
		let i = this.smi[mode];
		var notif_txt = this.ttmod + "<b>" + _(this.smn_t) + "</b> " + _("for") + " " + this.screen_name + ": " + this.tts;
		this._notifyMessage(i, notif_txt);
	},

	updateAll: function() {
		this.update_brightness_scroll(this.brightness);
		this.menu_sliders.update_items_brightness();
		this.menu_sliders.update_items_gamma();
		this.menu_sliders.updateCT(this.colortemp, this.gamma);
		this.menu_sliders.update_items_colortemp();
		this.menu_sliders.update_items_rgb();
		this.update_xrandr_outside(true);
		this.smn_t = this.smn[this.screen_mode];
		this.update_tooltip_values();
//		this.set_applet_tooltip(this.update_tooltip_values());
	},

//_check if there is any application associated with markdown files (.md)
	mdAssoc: function() {
		try {
			let kf = GLib.KeyFile.new();
			kf.load_from_file(GLib.get_user_config_dir() + "/mimeapps.list", 0);
//_why would they bother to implement g_key_file_has_key()... Ugh !!!
			let r = kf.get_string("Added Associations", "text/markdown", null);
//_Nope, there's no implementation of g_key_file_free()
//_so we use the next best thing
			kf.unref();
			if (this.dbgsw) global.log("markdown association file=" + r);
			return (r ? true : false);
		}
		catch(e) { global.logError(aSig + e); }
	},
/*====================== SETTINGS WINDOW ======================*/
	openSettings: function(params) {
		let w = this._chkWinOpen(this._meta.name);
		if (w) {
			w.raise_top();
			w.metaWindow.activate(global.get_current_time());
			return;
		}
		this.wait = global.display.connect_after('notify::focus-window', () => this._waitSettings(params));
		Util.spawnCommandLine("xlet-settings applet " + this._uuid + " " + this.instance_id);
	},

	_chkWinOpen: function(win) {
//_list all open windows, get their handles ?, loop through them searching for 'applet name' title
		let windows = global.get_window_actors();	// arrays of MetaWindowActor objects
		for (let i=0; i<windows.length; i++) {
// props: emit(), get_compositor_private(), get_monitor, get_workspace(), minimize, set_icon_geometry()
			let name= windows[i].metaWindow.get_title();
			if (name == this._meta.name) return windows[i];
		}
		return false;
	},

	_waitSettings: function(par) {
		try {
		let wind = this._chkWinOpen(this._meta.name);
		if (!wind || typeof wind == 'undefined') {
		global.display.disconnect(this.wait);
		return;
		}
		settingsWindow = wind.metaWindow;
// get_(preferred_)height, get_(preferred_)name, get_(preferred_)size, get_(preferred_)width, hide,
// set_height, set_size, set_width, set_position, show
// also check Mutter/Muffin metaWindow functions/properties/signals
		let x, y, w, h; let orect = new Meta.Rectangle();	// cinnamon --replace
		let win = settingsWindow.get_compositor_private();		// get MetaWindowActor object
		[orect.x, orect.y] = win.get_transformed_position();
		[orect.width, orect.height] = win.get_size();
//_we should also check if par values are numbers/integers !!!
		x = (par.x != 'undefined') ? par.x : orect.x;
		y = (par.y != 'undefined') ? par.y : orect.y;
		w = (par.width != 'undefined') ? par.width : orect.width;
		h = (par.height != 'undefined') ? par.height : orect.height;
		settingsWindow.move_resize_frame(false, x, y, w, h);
		settingsWindow.emit('position-changed');
		settingsWindow.emit('size-changed');
		} catch(e) { global.logError("Settings window error: " + e); }
		global.display.disconnect(this.wait);
	},
/*====================== UPDATE TOOLTIP =======================*/
	update_tooltip_values: function() {
/*
get_opacity(), set_opacity(), show(), set_style(), visible, get_preferred_size()
*/
		let a, asub, cld, d;
		let c = this._applet_tooltip._tooltip.get_theme_node().get_color('color');
		let clr = (c.red + c.green + c.blue) / 381 < 1.0 ? "#770077" : "#FFDDFF";
		let sf = "<span foreground='" + clr + "'>%s</span>";
		let df = "<span foreground='#FFFFFF' size='x-large'>%s</span>";
		let df2 = "<span foreground='#FFFFFF' size='large' rise='" + this.rise + "'>%s</span>";
		this.tthdr = "<span foreground='" + clr + "' size='medium'><b>%s %s</b></span>\n";
		this.default_tooltip = this.tthdr.format(_(this._meta.name), this._meta.version);
		if (this.decor) {
			cld = this.cloudy ? df.format(" \u{26C5}") : "";		// partly cloudy
			if (this.screen_mode == 3) {
				asub = "  " + (this.auto_mode == 2 ? "\u{1F310}" : "\u{1F3E1}");	// globe / home
				a = df2.format(asub);
				d = df.format(this._daySymbol(this.sun.cds, this.sun.tObj));
				d = d + "\t" + cld + "\t" + this.tock;
			} else { a = ""; d = df.format("\u{1F440}") + (this.cloudy ? "  " + cld : ""); }	// eyes
		} else {
			if (this.screen_mode == 3) {
				asub = " <i>" + (this.auto_mode == 2 ? this.ttaon : this.ttaof) + "</i>";
				a = sf.format(asub); d = sf.format(this.tock);
			} else { a = ""; d = " "; }
			cld = sf.format(" <i>" + this.ttcdy + "</i>");
		}

		let ttv = this.default_tooltip + d +
			"\n[ " + this.screen_name + ": " + sf.format(this.tts) +		// selected screen
			" ]\n" + this.ttmod + sf.format(this.smn_t) + a +				// current mode + auto submode
			"\n" + this.ttbri + sf.format(this.brightness + "%") +			// current brightness
			(this.cloudy && !this.decor ? cld : "") +							// cloudy mode
			"\n" + this.ttclr + sf.format(this.colortemp + "K") +			// current color temperature
			"\n" + this.ttgam + sf.format(this.gamma + "%") +			// current gamma
			"\nRGB: " + sf.format(this.gamma_red) +						// current RGB
			"." + sf.format(this.gamma_green) +
			"." + sf.format(this.gamma_blue);
		this._applet_tooltip._tooltip.get_clutter_text().set_markup(ttv);
		this._applet_tooltip._tooltip.clutter_text.set_markup(ttv);	// yes, it has to be duplicated!
		this._applet_tooltip._tooltip.set_opacity(255);
		this._applet_tooltip._tooltip.set_style(this.ttstyle);
		if (this.dbgsw) global.log(SEP + ttv.replace(/\n/g, " | "));
		return this._applet_tooltip._tooltip.get_clutter_text();
	},

	_daySymbol: function(ds, o) {
	let t = new Date().getTime();
	let cd = new Date().setHours(0, 0, 0, 0);
	let od = new Date().setHours(0, this.shift_day, 0) - cd;
	let on = new Date().setHours(0, this.shift_night, 0) - cd;
	if (ds==1) return "\u{1F319}";								// city star night [ \u1F319 ][ \u1F303 ]
	if (ds==2) return "\u{1F318}";								// nautical twilight begin [ \u1F318 ]
	if (ds==3 && t <= (o[1] - od)) return "\u{1F317}";	// civil twilight begin [ \u1F317 ]
	if (ds==3 && t <= o[1]) return "\u{1F305}";			// sunrise [ \u1F305 ]
	if (ds==4) return "\u{1F31E}";								// sun face [ \u1F31E ]
	if (ds==5 && t <= (o[2] + on)) return "\u{1F307}";	// sunset [ \u1F307 ]
	if (ds==5 && t <= o[3]) return "\u{1F313}";			// civil twilight end [ \u1F313 ]
	if (ds==6) return "\u{1F312}";								// nautical twilight end [ \u1F312 ]
	return "\u{1F308}";	// rainbow
	},

	tickTock: function() {
		if (!this._applet_tooltip._tooltip.visible) return;
		this.tick = !this.tick;
		this.tock = (this.decor) ?
			this.SymStyle.format(this.tick ? "\u{1F49C}" : "\u{1F493}") :	// 1F49C=purple heart
			(this.tick ? "\u25CF" : "\u25CB");			// \u{1F43E} paws
		this.update_tooltip_values();
	},
/*====================== UPDATE SUNDATA =======================*/
	updateSundata: function(val) {
		if (this.dbgsw) global.log("updateSundata, locupdate=" + this.locupdate + ", val=" + val);
		if (val == false) return;	// workaround for double call on button toggle on/off
		let d = (new Date()).toLocaleDateString(this.auto_fmt, {timeZone: this.auto_tz});
		let t = (new Date()).toLocaleTimeString(this.auto_fmt, {timeZone: this.auto_tz});
		this.lastdate = d + ", " + t;
		if (!this.sun.updateData(2)) {	// send mode 2 (online) to bypass internal function check
			this.locinfo = _("Error retrieving data.\nPlease try again later.");
			this.loclast = this.lastdate;
		} else {
			this.sun.getDayslice(); this.refreshLocinfo(this.sun.cds, this.sun.wallet, this.sun.msg);
		}
		this.unlockTimer = Util.setTimeout(() => this._unlock(this), 5000);
	},

	refreshLocinfo: function(c, g, m) {
		this.lastdate = g.upd;
		let la = (g.lat < 0) ? "S, " : "N, "; let lo = (g.lon < 0) ? "W " : "E ";
		this.locinfo = _("Public IP: ") + g.ip +
		_("\nTime zone: ") + g.tzn +
		_("\nGeolocation: ") + g.lat + "° " + la + g.lon + "° " + lo +
		_("\nNautical twilight begin: ") + g.ntb +
		_("\nCivil twilight begin: ") + g.ctb +
		_("\nSunrise: ") + g.snr +
		_("\nSunset: ") + g.sns +
		_("\nCivil twilight end: ") + g.cte +
		_("\nNautical twilight end: ") + g.nte +
		_("\nDay length: ") + g.dln +
		_("\n\nWe are now in time slice: ") + c +
		"\n[ " + m[c - 1] + _(" to ") + m[c] + " ]\n" +
		SEP0 + _("\nConnection details:\n\nISP: ") + g.isp +
		_("\nCity: ") + g.cit +
		_("\nCountry: ") + g.cty;
	},

	_unlock: function(v) {
		let that = v;
		that.settings.setValue("loclast", that.lastdate);
		try { that.settings.setValue("locinfo", that.locinfo); } catch(e) { global.log("setValue locinfo, error: " + e); }
		try { that.settings.setValue("locupdate", false); } catch(e) { global.log("setValue locupdate, error: " + e); }
		if (that.unlockTimer)
			Util.clearTimeout(that.unlockTimer);
		that.unlockTimer = null;
		return false;
	},
/*============= ICON ANIMATION for AUTO ONLINE mode ===========*/
	animicon: function(mode) {
		if (mode && !this.animID) this.animID = Util.setInterval(() => this.animate(), 120);
		else if (!mode && this.animID) {
			Util.clearInterval(this.animID);
			this.animID = null;
			this.set_gui_icon(this.screen_mode);
		}
	},
/*==== adapted from SpicesUpdate by claudiux => applet.js =====*/
	animate: function() {
		this.angle = Math.round(this.angle + 3) % 360;
		let size = Math.round(this.getPanelIconSize(St.IconType.SYMBOLIC) * global.ui_scale);
		this.img_icon = this.getImageAtScale(this.autopath, size, size);
		this.img_icon.set_pivot_point(0.5, 0.5);
		this.img_icon.set_rotation_angle(Clutter.RotateAxis.Z_AXIS, this.angle);
		this._applet_icon_box.set_child(this.img_icon);
		let h = this.isHorizontal;
		this._applet_icon_box.set_fill(!!h, !h);
		this._applet_icon_box.set_alignment(St.Align.MIDDLE,St.Align.MIDDLE);
	},

	getImageAtScale: function(imageFileName, width, height) {
		let pixBuf = GdkPixbuf.Pixbuf.new_from_file_at_size(imageFileName, width, height);
		let image = new Clutter.Image();
		image.set_data(
			pixBuf.get_pixels(),
			pixBuf.get_has_alpha() ? Cogl.PixelFormat.RGBA_8888 : Cogl.PixelFormat.RGBA_888,
			width, height,
			pixBuf.get_rowstride()
		);
		let actor = new Clutter.Actor({width: width, height: height});
		actor.set_content(image);
		return actor;
	},
/*==================== POPUP NOTIFICATION =====================*/
/*============= adapted from betterlock => applet.js ==========*/
	_ensureSource: function() {
		if (!this._source) {
			this._source = new MessageTray.Source();
			this._source.connect('destroy', () => this._source = null );
			if (Main.messageTray) Main.messageTray.add(this._source);
		}
	},

	_notifyMessage: function(iconName, text) {
		if (this._notification) this._notification.destroy();
		if (this.popup == false) return;
		this._ensureSource();	// avoid clearing this._source after destroying previous notification
		let icon = new St.Icon({
			icon_name: iconName,
			icon_type: St.IconType.FULLCOLOR,		// originally was SYMBOLIC
			icon_size: ICON_SIZE
		});
		try {
			this._notification = new MessageTray.Notification(
				this._source, _(this._meta.name), text, {icon: icon, bannerMarkup: true});
		} catch (e) {
			this._notification = new MessageTray.Notification(
				this._source, _(this._meta.name), text, {icon: icon, titleMarkup: true, bodyMarkup: true}); }
//		this._notification.addBody("extra text for <b>testing</b>", true, "");
		this._notification.setUrgency(MessageTray.Urgency.NORMAL);
		this._notification.setTransient(true);
		this._notification.connect('destroy', function() { this._notification = null; });
		this._source.notify(this._notification);
	},
};
/*=================== MAIN SCRIPT FUNCTION ====================*/
function main(metadata, orientation, panel_height, instance_id) {
	return new AppletPYE(metadata, orientation, panel_height, instance_id);
}

